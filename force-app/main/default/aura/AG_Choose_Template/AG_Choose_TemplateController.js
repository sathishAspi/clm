({
	doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getObjectTemplateList");
        action.setParams ({ "recordId" : recordId });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if(state == "SUCCESS"){
                var templateObjList = JSON.parse(response.getReturnValue());
                component.set("v.templateObjectList",templateObjList);
                var templateObjListWrapper = component.get("v.templateObjListWrapper")
                templateObjList.forEach(function(x) {
                    var template = {"isSelected": false,"templateObj":x};
                    templateObjListWrapper.push(template);
                });  
                component.set("v.templateObjListWrapper",templateObjListWrapper);
                
            }
        });
        $A.enqueueAction(action);
        
	},
    navToRecord:function(component, event, helper){
        var recordId = component.get("v.recordId");
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId
        });
        navEvt.fire();  
    },
    callDocGeneration: function(component, event, helper){
        component.set("v.isLoading",true);
        var recordId = component.get("v.recordId");
        var templateObjListWrapper = component.get("v.templateObjListWrapper");
        var selectedTemplateObj = '';
        templateObjListWrapper.forEach(function(x) {
            if(x.isSelected == true) {
                selectedTemplateObj = x.templateObj ;
            }
            }); 
        if(selectedTemplateObj){
        var action = component.get("c.callGenerateDocument");
        action.setParams ({ "selectedTemplateObj" : JSON.stringify(selectedTemplateObj),
                           	"recordId" : recordId});
        action.setCallback(this,function(response) {
            var state = response.getState();
            if(state == "SUCCESS"){
                var res = JSON.parse(response.getReturnValue());
                console.log("responseValue-->"+response.getReturnValue());
                var toastEvent = $A.get("e.force:showToast");
                var navEvt = $A.get("e.force:navigateToSObject");
                if(res.success) {
                    
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": res.message
                    });
                    toastEvent.fire();
                    
                    navEvt.setParams({
                        "recordId": recordId
                    });
                    navEvt.fire();
                }
                else {
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": res.message
                    });
                    toastEvent.fire();
                    navEvt.setParams({
                        "recordId": recordId
                    });
                    navEvt.fire();
                }
                
            }
        }); 
        $A.enqueueAction(action);
        }else{
           component.set("v.isLoading",false);
           var toastErrorEvent = $A.get("e.force:showToast");
                    toastErrorEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Please choose a template!"
                    });
                    toastErrorEvent.fire(); 
        }
    }
})