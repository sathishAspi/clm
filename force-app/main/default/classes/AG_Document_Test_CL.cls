@isTest
public with sharing class AG_Document_Test_CL {
    
    @isTest static void callInitGetContacts(){ 
        User contractManageruser = AG_Document_Test_Data.createTestUser();
        AG_Document_Test_Data.createPermissionSetForAdmin(contractManageruser.Id);
        
        System.runAs(contractManageruser) { 
            Account account = AG_Document_Test_Data.createAccount();
            Contact contact = AG_Document_Test_Data.createContact(account);
            AG_Agreement_Days_Price__c agreementDaysPrice = AG_Document_Test_Data.createAgreementDaysPrice();
            AG_Agreement__c agreement = AG_Document_Test_Data.createAgreement(account, agreementDaysPrice);
            insert agreement;
            ContentVersion contentVersion = AG_Document_Test_Data.createContentVersion();
            ContentDocumentLink contentDoclink = AG_Document_Test_Data.createContentDocLink(agreement);
            Test.startTest();
            String result = AG_Document_CL.getContacts(agreement.Id);
            Test.stopTest();
            System.assertEquals(result.contains('Id'), true);
        }
    }
    
    @isTest static void callActivateContract(){
        User contractManageruser = AG_Document_Test_Data.createTestUser();
        AG_Document_Test_Data.createPermissionSetForAdmin(contractManageruser.Id);
        
        System.runAs(contractManageruser) {
            Account account = AG_Document_Test_Data.createAccount();
            Contact contact = AG_Document_Test_Data.createContact(account);
            AG_Agreement_Days_Price__c agreementDaysPrice = AG_Document_Test_Data.createAgreementDaysPrice();
            AG_Agreement__c agreement = AG_Document_Test_Data.createAgreement(account, agreementDaysPrice);
            agreement.AG_Status__c = 'In Signature';
            insert agreement;
            Test.startTest();
            String actualResult = AG_Document_CL.activateContract(agreement.Id);
            String expectedResult = '{"success": true, "message": "Contract activated successfully"}';
            Test.stopTest();
            System.assertEquals(expectedResult, actualResult);
        }
    }
    
    @isTest static void callFindAgreement(){
        User contractManageruser = AG_Document_Test_Data.createTestUser();
        AG_Document_Test_Data.createPermissionSetForAdmin(contractManageruser.Id);
        
        System.runAs(contractManageruser) {
            Account account = AG_Document_Test_Data.createAccount();
            Contact contact = AG_Document_Test_Data.createContact(account);
            AG_Agreement_Days_Price__c agreementDaysPrice = AG_Document_Test_Data.createAgreementDaysPrice();
            AG_Agreement__c agreement = AG_Document_Test_Data.createAgreement(account, agreementDaysPrice);
            insert agreement;
            String actualResult = AG_Document_CL.findAgreement(agreement.Id);
            System.assertEquals(actualResult.contains('Id'), true);
        }
    }
    
    @isTest static void callCreateTask(){
        User contractManageruser = AG_Document_Test_Data.createTestUser();
        AG_Document_Test_Data.createPermissionSetForAdmin(contractManageruser.Id);
        
        System.runAs(contractManageruser) {
            Account account = AG_Document_Test_Data.createAccount();
            Contact contact = AG_Document_Test_Data.createContact(account);
            AG_Agreement_Days_Price__c agreementDaysPrice = AG_Document_Test_Data.createAgreementDaysPrice();
            AG_Agreement__c agreement = AG_Document_Test_Data.createAgreement(account, agreementDaysPrice);
            insert agreement;
            AG_Document_CL.createTask(agreement.Id);
            System.assertEquals(agreement.AG_Agreement_Value__c, 500);
        }
    }
    
    @isTest static void callValidateUpload(){
        User contractManageruser = AG_Document_Test_Data.createTestUser();
        AG_Document_Test_Data.createPermissionSetForAdmin(contractManageruser.Id);
        
        System.runAs(contractManageruser) {
            Boolean isUpload = true;
            Account account = AG_Document_Test_Data.createAccount();
            Contact contact = AG_Document_Test_Data.createContact(account);
            AG_Agreement_Days_Price__c agreementDaysPrice = AG_Document_Test_Data.createAgreementDaysPrice();
            AG_Agreement__c agreement = AG_Document_Test_Data.createAgreement(account, agreementDaysPrice);
            insert agreement;
            String expectedResult = '{"success": false, "data": '+isUpload+'}';
            String actualResult =  AG_Document_CL.validateUpload(agreement.Id);
            System.assertEquals(expectedResult, actualResult);
        }
    }
    
    @isTest static void callBackProcess(){
        User contractManageruser = AG_Document_Test_Data.createTestUser();
        AG_Document_Test_Data.createPermissionSetForAdmin(contractManageruser.Id);
        
        System.runAs(contractManageruser) {
            Account account = AG_Document_Test_Data.createAccount();
            Contact contact = AG_Document_Test_Data.createContact(account);
            AG_Agreement_Days_Price__c agreementDaysPrice = AG_Document_Test_Data.createAgreementDaysPrice();
            AG_Agreement__c agreement = AG_Document_Test_Data.createAgreement(account, agreementDaysPrice);
            agreement.AG_Status__c = 'Activated';
            insert agreement;
            String expectedResult = '{"success": true, "message": "Action performed successfully"}';
            String actualResultOne =  AG_Document_CL.performBackProcess(agreement.Id, 'In Signature', 'test');
            String actualResultTwo =  AG_Document_CL.performBackProcess(agreement.Id, 'In Approval', 'test');
            String actualResultThree =  AG_Document_CL.performBackProcess(agreement.Id, 'In Review', 'test');
            String actualResultFour =  AG_Document_CL.performBackProcess(agreement.Id, 'Requested', 'test');
            System.assertEquals(expectedResult, actualResultOne);
        }
    }
    
    @isTest static void callSendForReview(){
        User contractManageruser = AG_Document_Test_Data.createTestUser();
        AG_Document_Test_Data.createPermissionSetForAdmin(contractManageruser.Id);
        
        System.runAs(contractManageruser) {
            List<Contact> contactList = new List<Contact>();
            Account account = AG_Document_Test_Data.createAccount();
            Contact contact = AG_Document_Test_Data.createContact(account);
            contactList.add(contact);
            
            AG_Agreement_Days_Price__c agreementDaysPrice = AG_Document_Test_Data.createAgreementDaysPrice();
            AG_Agreement__c agreement = AG_Document_Test_Data.createAgreement(account, agreementDaysPrice);
            insert agreement;
            ContentVersion contentVersion = AG_Document_Test_Data.createContentVersion();
            ContentDocumentLink contentDoclink = AG_Document_Test_Data.createContentDocLink(agreement);
            ContentDocumentLink contentDoc = [SELECT LinkedEntityid, ContentDocumentid FROM contentDocumentLink WHERE Id =: contentDocLink.Id];
            
            List<String> toAddressList = new List<String>{'test@email.com'};
                List<String> ccAddressList = new List<String>{'test@test.com', 'test@testOrg.com'};
                    
                    List<ContentVersion> contentVersionList = [SELECT Title,IsLatest,FileExtension,ContentDocumentId,CreatedBy.Name,CreatedDate FROM ContentVersion WHERE ContentDocumentId = : contentDoc.ContentDocumentId AND IsLatest =: true];
            List<EmailTemplate> emailTemplateList = new List<EmailTemplate>();
            EmailTemplate emailTemplate;
            System.runAs (new User(Id = UserInfo.getUserId()) ){
                emailTemplate = new EmailTemplate();
                emailTemplate.isActive = true;
                emailTemplate.Name = 'name';
                emailTemplate.DeveloperName = 'unique_name';
                emailTemplate.TemplateType = 'text';
                emailTemplate.Subject = 'Test';
                emailTemplate.Body = 'Test';
                emailTemplate.HtmlValue = 'Test';
                emailTemplate.FolderId = UserInfo.getUserId();
                insert emailTemplate;
            }
            emailTemplateList.add(emailTemplate);
            String expectedResult = '{"success": true, "message": "Email Sent successfully"}';
            String actualResult = AG_Document_CL.sendForReview(agreement.Id, JSON.serialize(contactList), JSON.serialize(contentVersionList), JSON.serialize(emailTemplateList), JSON.serialize(toAddressList), JSON.serialize(ccAddressList), emailTemplate.Id, '', '');
            System.assertEquals(expectedResult, actualResult);
        }
    }
    @isTest static void callSendForSignature(){
        User contractManageruser = AG_Document_Test_Data.createTestUser();
        AG_Document_Test_Data.createPermissionSetForAdmin(contractManageruser.Id);
        
        System.runAs(contractManageruser) {
            List<Contact> contactList = new List<Contact>();
            Account account = AG_Document_Test_Data.createAccount();
            Contact contact = AG_Document_Test_Data.createContact(account);
            contactList.add(contact);
            
            AG_Agreement_Days_Price__c agreementDaysPrice = AG_Document_Test_Data.createAgreementDaysPrice();
            AG_Agreement__c agreement = AG_Document_Test_Data.createAgreement(account, agreementDaysPrice);
            insert agreement;
            ContentVersion contentVersion = AG_Document_Test_Data.createContentVersion();
            ContentDocumentLink contentDoclink = AG_Document_Test_Data.createContentDocLink(agreement);
            ContentDocumentLink contentDoc = [SELECT LinkedEntityid, ContentDocumentid FROM contentDocumentLink WHERE Id =: contentDocLink.Id];
            
            List<String> toAddressList = new List<String>{'test@email.com'};
                List<String> ccAddressList = new List<String>{'test@test.com', 'test@testOrg.com'};
                    
                    List<ContentVersion> contentVersionList = [SELECT Title,IsLatest,FileExtension,ContentDocumentId,CreatedBy.Name,CreatedDate FROM ContentVersion WHERE ContentDocumentId = : contentDoc.ContentDocumentId AND IsLatest =: true];
            List<EmailTemplate> emailTemplateList = new List<EmailTemplate>();
            EmailTemplate emailTemplate;
            System.runAs (new User(Id = UserInfo.getUserId()) ){
                emailTemplate = new EmailTemplate();
                emailTemplate.isActive = true;
                emailTemplate.Name = 'name';
                emailTemplate.DeveloperName = 'unique_name_addSomethingSpecialHere';
                emailTemplate.TemplateType = 'text';
                emailTemplate.Subject = 'Test';
                emailTemplate.Body = 'Test';
                emailTemplate.HtmlValue = 'Test';
                emailTemplate.FolderId = UserInfo.getUserId();
                insert emailTemplate;
            }
            emailTemplateList.add(emailTemplate);
            String expectedResult = '{"success": true, "message": "Email Sent successfully"}';
            String actualResult = AG_Document_CL.sendForSignature(agreement.Id, JSON.serialize(contactList), JSON.serialize(contentVersionList), JSON.serialize(emailTemplateList), JSON.serialize(toAddressList), JSON.serialize(ccAddressList), emailTemplate.Id, '', '');
            System.assertEquals(expectedResult, actualResult);
        }
    }
    
    @isTest static void callcheckBackProcess(){
        User contractManageruser = AG_Document_Test_Data.createTestUser();
        AG_Document_Test_Data.createPermissionSetForAdmin(contractManageruser.Id);
        
        System.runAs(contractManageruser) {
            List<Contact> contactList = new List<Contact>();
            Account account = AG_Document_Test_Data.createAccount();
            Contact contact = AG_Document_Test_Data.createContact(account);
            contactList.add(contact);
            
            AG_Agreement_Days_Price__c agreementDaysPrice = AG_Document_Test_Data.createAgreementDaysPrice();
            AG_Agreement__c agreement = AG_Document_Test_Data.createAgreement(account, agreementDaysPrice);
            insert agreement;
            
            String expectedResult = '{"success": false, "message": "The contract is in its initial stage. Back process can\'t be performed at this stage"}';
            String actualResult = AG_Document_CL.checkBackProcessIsValid(agreement.Id);
            System.assertEquals(expectedResult, actualResult);
        }
    }
    
    @isTest static void callDocTriggerForApproval(){
        User contractManageruser = AG_Document_Test_Data.createTestUser();
        AG_Document_Test_Data.createPermissionSetForAdmin(contractManageruser.Id);
        
        System.runAs(contractManageruser) {
            Account account = AG_Document_Test_Data.createAccount();
            Contact contact = AG_Document_Test_Data.createContact(account);
            AG_Agreement_Days_Price__c agreementDaysPrice = AG_Document_Test_Data.createAgreementDaysPrice();
            AG_Agreement__c agreement = AG_Document_Test_Data.createAgreement(account, agreementDaysPrice);
            insert agreement;
            
            Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
            request.setComments('Submitting request for approval.');
            request.setObjectId(agreement.Id);
            request.setSubmitterId(UserInfo.getUserId()); 
            request.setProcessDefinitionNameOrId('AG_Agreement_Approval_Process');
            request.setSkipEntryCriteria(true);
            Approval.ProcessResult result = Approval.process(request);
            System.assertEquals(agreement.AG_Is_Changed__c, false);
        }
    }
    
    @isTest static void callDocTriggerForActivation(){
        User contractManageruser = AG_Document_Test_Data.createTestUser();
        AG_Document_Test_Data.createPermissionSetForAdmin(contractManageruser.Id);
        
        System.runAs(contractManageruser) {
            Account account = AG_Document_Test_Data.createAccount();
            Contact contact = AG_Document_Test_Data.createContact(account);
            AG_Agreement_Days_Price__c agreementDaysPrice = AG_Document_Test_Data.createAgreementDaysPrice();
            AG_Agreement__c agreement = AG_Document_Test_Data.createAgreement(account, agreementDaysPrice);
            insert agreement;
            agreement.AG_Status__c = 'In Signature';
            update agreement;
            AG_Document_CL.activateContract(agreement.Id);
            System.assertEquals(agreement.AG_Is_Changed__c, false);
        }
    }
}