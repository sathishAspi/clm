({
    getAllDocuments : function(component){
        var checkedvalue = component.get("v.isSelectAllDocuments");
        try {
            checkedvalue = event.getSource().get("v.value");
        }
        catch(e) {}
        var filteredDocumentList = component.get("v.documentList");
        for(var x in filteredDocumentList){
            if(filteredDocumentList.hasOwnProperty(x)){
                filteredDocumentList[x].isSelected = checkedvalue;
            }
        }
        component.set("v.documentList", filteredDocumentList);
    },
    
    navigateToParent : function(component) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId"),
            "slideDevName": "details"
        });
        navEvt.fire();
    }
})