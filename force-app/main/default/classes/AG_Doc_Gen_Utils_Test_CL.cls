@isTest
public class AG_Doc_Gen_Utils_Test_CL {
    @isTest static void getObjectListTest() {
        Test.startTest();
        String objList = AG_Document_Generation_Utils_CL.getObjectList();
        System.assertEquals(objList,objList);
        System.assertNotEquals(objList,null);
        Test.stopTest();
    }
    @isTest static void getObjectListMapTest() {
        Test.startTest();
        String objList = AG_Document_Generation_Utils_CL.getObjectListMap();
        System.assertEquals(objList,objList);
        System.assertNotEquals(objList,null);
        Test.stopTest();
    }
    @isTest static void getChildObjListTest(){
        String childObjectList = AG_Document_Generation_Utils_CL.getChildObjectList('Account');
        System.assertEquals(childObjectList,childObjectList);
        System.assertNotEquals(childObjectList,null);
        
    }
    @isTest static void getObjectApiNameTest(){
        String objectApiName = AG_Document_Generation_Utils_CL.getObjectApiName('Contact','AccountId');
        System.assertEquals(objectApiName,objectApiName);
        System.assertNotEquals(objectApiName,null);
    }
    @isTest static void isReferenceFieldTest(){
        Boolean isRefField = AG_Document_Generation_Utils_CL.isReferenceField('Contact','AccountId');
        System.assertEquals(isRefField,isRefField);
        System.assertNotEquals(isRefField,null);
    }
    @isTest static void saveTemplateObjectTest(){
        String templateObj = AG_Doc_Gen_Test_Data_CL.createTemplate();
        String templateObject = AG_Document_Generation_Utils_CL.saveTemplateObject(templateObj);
        System.assertEquals(templateObject,templateObject);
        System.assertNotEquals(templateObject,null);
    }
    @isTest static void getObjectTemplateListTest(){
        String templateObj = AG_Doc_Gen_Test_Data_CL.createTemplate();
        AG_Template__c templateObject = (AG_Template__c)System.JSON.deserialize(templateObj, AG_Template__c.class);
        insert templateObject;
        String recordId = AG_Doc_Gen_Test_Data_CL.createAccount();
        String templateObjList = AG_Document_Generation_Utils_CL.getObjectTemplateList(recordId);
        System.assertEquals(templateObjList,templateObjList);
        System.assertNotEquals(templateObjList,null);
    }
    @isTest static void callGenerateDocumentTest(){
        String templateObj = AG_Doc_Gen_Test_Data_CL.createTemplate();
        AG_Template__c templateObject = (AG_Template__c)System.JSON.deserialize(templateObj, AG_Template__c.class);
        insert templateObject;  
        String recordId = AG_Doc_Gen_Test_Data_CL.createAccount();
        ContentVersion contentVer = new ContentVersion(Title = 'Test',
                                                       PathOnClient = 'Test.jpg',
                                                       VersionData = Blob.valueOf('Test Content Data'),
                                                       IsMajorVersion = true,
                                                      FirstPublishLocationId =templateObject.Id );
        insert contentVer;
        String message = AG_Document_Generation_Utils_CL.callGenerateDocument(JSON.serialize(templateObject),recordId);
        System.assertEquals(message,message);
        System.assertNotEquals(message,null);
    }
    @isTest static void generateQueryTest(){
        String testJson = '{"objName": "Account", "isExists": false, "fieldWrapperList": [{"fieldName": "Name", "isExists": false}, {"fieldName": "OperatingHoursId", "isExists": false}, {"fieldName": "OperatingHoursId", "isExists": false}], "childObjWrapperList": [{"objName": "Contacts", "isExists": false, "fieldWrapperList": [{"fieldName": "Name", "isExists": false}, {"fieldName": "Email", "isExists": false}, {"fieldName": "AccountId", "isExists": false}], "parentObjWrapperList": [{"objName": "AccountId", "isExists": false, "fieldWrapperList": [{"fieldName": "BillingCountry", "isExists": false}, {"fieldName": "OperatingHoursId", "isExists": false}], "childObjWrapperList": [], "parentObjWrapperList": [], "grandObjWrapperList": [{"objName": "OperatingHoursId", "isExists": false, "fieldWrapperList": [{"fieldName": "Name", "isExists": false}]}]}], "QueryCondition": ""}], "parentObjWrapperList": [{"objName": "OperatingHoursId", "isExists": false, "fieldWrapperList": [{"fieldName": "Name", "isExists": false}, {"fieldName": "CreatedById", "isExists": false}], "childObjWrapperList": [], "parentObjWrapperList": [], "grandObjWrapperList": [{"objName": "CreatedById", "isExists": false, "fieldWrapperList": [{"fieldName": "Name", "isExists": false}]}]}]}';
        String recordId = AG_Doc_Gen_Test_Data_CL.createAccountDataForDocGen();
        String jsonData = AG_Document_Generation_Utils_CL.generateQuery(testJson, recordId);
        System.assertEquals(jsonData,jsonData);
        System.assertNotEquals(jsonData,null);
    }
}