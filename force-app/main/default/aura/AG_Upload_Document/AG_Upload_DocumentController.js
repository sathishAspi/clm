({
    doInit : function(component, event, helper){
        component.set("v.toggleSpinner", true);
        var toastEvent = $A.get("e.force:showToast");
        var action = component.get("c.validateUpload");
        action.setParams({
            "recordId" : component.get("v.recordId"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.toggleSpinner", false);
            if(state == 'SUCCESS'){
                var res = JSON.parse(response.getReturnValue());
                if(res.success){
             /*       $A.get("e.force:closeQuickAction").fire();
                    component.set("v.hasNoError" , false);
                    toastEvent.setParams({ "title": "Error!", "type": "Error", "message": res.message });
                    toastEvent.fire();  */
                     component.set("v.hasNoError" , true);
                }
                else{
                    component.set("v.hasNoError" , true);
                }
            }
            else{
                toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "Something went wrong" });
                toastEvent.fire(); 
            }
            
        });
        $A.enqueueAction(action);
    },
    
    closeModel : function(component, event, helper) {
        component.set("v.isUpload" , false);
        component.set("v.isReview" , false);
    },
    
    handleUploadFinished : function(component, event, helper) {
        var uploadedFiles = event.getParam("files");
        component.set("v.file" , uploadedFiles);
        var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
        if(fileName != '' || fileName != null){
            component.set("v.fileName" , fileName);
        }
        $A.get("e.force:closeQuickAction").fire();
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": "Success!", "type": "Success", "message": "File "+fileName+" Uploaded successfully." });
        toastEvent.fire();
        var action = component.get("c.createTask");
        action.setParams({
            "recordId" : component.get("v.recordId"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == 'SUCCESS'){
                location.reload();
            }
        });
        $A.enqueueAction(action);
    },
})