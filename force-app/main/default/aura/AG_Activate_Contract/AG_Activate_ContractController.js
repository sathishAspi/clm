({
    doInit : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        component.set("v.isReview" , true);
        var action = component.get("c.activateContract");
        action.setParams({
            "recordId" : component.get("v.recordId"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.toggleSpinner", false);
            $A.get("e.force:closeQuickAction").fire();
            if(state === "SUCCESS") {
                var res = JSON.parse(response.getReturnValue());
                if(res.success){
                    location.reload();
                    toastEvent.setParams({ "title": "Success!", "type": "Success", "message": res.message });
                    toastEvent.fire();
                }
                else{
                    toastEvent.setParams({ "title": "Error!", "type": "Error", "message": res.message });
                    toastEvent.fire();
                }
            }
            else{
                toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "Something went wrong" });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    }
})