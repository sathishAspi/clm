({
    getObjFields : function(component) {
        var selectedObjectName = component.get("v.parentObject");
        var action = component.get("c.getParentObjFieldList"); 
        action.setParams ({ "objName" : selectedObjectName });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if(state == "SUCCESS"){
                var objFieldsList = JSON.parse(response.getReturnValue());
                objFieldsList.forEach(function(x) {
                    if(x.Type == 'REFERENCE') {
                        x.Label = x.Label + ' >';
                    } 
                });
                console.log("childObjList-->"+JSON.stringify(objFieldsList));
                component.set("v.parentObjectFieldsList",objFieldsList);
            }
        });
        $A.enqueueAction(action);
    },
    
    getChildObjectList : function(component) {
        try {
            var listData = component.get("v.objList");
            var res = listData.filter(function(x) {
                return x.value == component.get("v.parentObject");
            });
            component.set("v.pObjLabel",res[0].label);
            component.set("v.sourceObjectLabel",res[0].label);
            component.set("v.showParentObject" ,false);
            component.set("v.showChildObjects" ,true);
            component.set("v.isLoading",true);
            component.set("v.pObjName" ,component.get("v.parentObject"));
            this.getObjFields(component);
            var selectedObjectName = component.get("v.parentObject");
            var action = component.get("c.getChildObjList"); 
            action.setParams ({ "objName" : selectedObjectName });
            action.setCallback(this,function(response) {
                var state = response.getState();
                if(state == "SUCCESS"){
                    var childObjList = JSON.parse(response.getReturnValue());
                    console.log("childObjList-->"+JSON.stringify(childObjList));
                    component.set("v.childObjectList" , childObjList);
                    component.set("v.isLoading",false);
                }
            });
            component.set("v.selectedFieldSet",component.get("v.parentObject"));
            component.set("v.finalSelectedFields",'${' +component.get("v.selectedFieldSet") + '}');
            $A.enqueueAction(action);
        }
        catch(err) {
            console.log('Error-->' +err);
            console.log('Error-1->' +err.message);
        }
    },
    
    getObjectList : function(component, callChildFunction) {
        var action = component.get("c.getObjectList");
        action.setCallback(this,function(response) {
            var state = response.getState();
            if(state == "SUCCESS"){
                var objectList = JSON.parse(response.getReturnValue());
                component.set("v.objList" , objectList);
                component.set("v.isLoading ", false);
                
                if(callChildFunction) {
                    this.getChildObjectList(component);
                }
            }
        });
        $A.enqueueAction(action);
    }
})