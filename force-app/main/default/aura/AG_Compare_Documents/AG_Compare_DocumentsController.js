({
    doInit : function(component, event, helper) {
        component.set("v.toggleSpinner", true);
        var recordId = component.get("v.recordId");
        var toastEvent = $A.get("e.force:showToast");
        var action = component.get("c.getDocuments");
        action.setParams({
            "recordId" : component.get("v.recordId"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.toggleSpinner" , false);
            if(state == 'SUCCESS'){
                var res = JSON.parse(response.getReturnValue());
                if(res.success){
                    component.set("v.documentList" , res.data);
                    console.log(JSON.stringify(res.agreement));
                    component.set("v.agreementObject",res.agreement);
                    component.set("v.objectLabel" , res.objLabel);
                    toastEvent.setParams({ "title": "Success!", "type": "Success", "message": res.message });
                    toastEvent.fire();
                }
                else{
                    toastEvent.setParams({ "title": "Error!", "type": "Error", "message": res.message });
                    toastEvent.fire();
                }
            }
            else{
                toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "Something went wrong" });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    getDocument : function(component, event, helper){
        var documentList = component.get("v.documentList");
        var document = component.find("documentOne").get("v.value");
        var selectedDocument = documentList.filter(doc => doc.Title == document)[0];
        component.set("v.selectedDocumentOne" , selectedDocument);
    },
    
    getOtherDocument : function(component, event, helper){
        helper.getSelectedDocument(component);
    },
    
    callCancel : function(component, event, helper){
        helper.navigate(component);
    },
    
    callCompare : function(component, event, helper){
        var selectedDocumentOne = component.get("v.selectedDocumentOne");
        var selectedDocumentTwo = component.get("v.selectedDocumentTwo");
        component.set("v.toggleSpinner", true);
        var toastEvent = $A.get("e.force:showToast");
        var action = component.get("c.compareDocuments");
        action.setParams({
            "recordId" : component.get("v.recordId"),
            "documentOne" : JSON.stringify(selectedDocumentOne),
            "documentTwo" : JSON.stringify(selectedDocumentTwo)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
			component.set("v.toggleSpinner", false);
            if(state == 'SUCCESS'){
                var res = JSON.parse(response.getReturnValue());
                if(res.success){
                    console.log("res.data=======>"+res.data)
                    component.set("v.contentUrl" , res.data);
                    /*var vfUrl = component.get("v.contentUrl");
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": vfUrl
                    });
                    urlEvent.fire();*/
                    //window.open(component.get("v.contentUrl"));
                   // window.open("https://api.draftable.com/v1/comparisons/viewer/kwfKrQ-test/unuckzaL");
                } else{
                    toastEvent.setParams({ "title": "Error!", "type": "Error", "message": res.message });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },
    addNewFeatureMessage :function(component, event, helper){
        var toastEvent = $A.get("e.force:showToast");
         toastEvent.setParams({ "title": "Error!", "type": "Error", "message":"This feature is not available in this plan. Please contact support@aspigrow.com to upgrade your plan!" });
         toastEvent.fire();
    },
})