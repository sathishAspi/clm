public with sharing class AG_Document_Gen_FLS_CL {
    private AG_Document_Gen_FLS_CL() {
        
    }
    public static Boolean checkObjectAccess(String objName , String accessType) {
        String message;
        List<String> objList = new List<String>();
        Boolean isTrue = true;
        SObjectType sObjType = Schema.getGlobalDescribe().get(ObjName);
        if(accessType.toLowerCase() == 'read') {
            isTrue = (isTrue && sObjType.getDescribe().isAccessible());
            message = (isTrue) ? '' :System.Label.AG_security_error_object_not_readable;
        }
        objList.add(ObjName);
        if(!isTrue){
            throw new securityException(String.format(message, objList)) ;
        }
        return isTrue; 
    }
    public static List<String> getFieldsWithAccess( String ObjName , String accessType, List<String> fieldList ) {
        List<String> returnFieldList = new List<String>();
        
        
        SObjectType sObjType = Schema.getGlobalDescribe().get(ObjName);
        Map<String , Schema.SObjectField> fieldMap = sObjType.getDescribe().fields.getMap();
        
        for(String field : fieldList){
            
            if(accessType.toLowerCase() == 'read' && fieldMap.get(field.trim()).getDescribe().isAccessible()) {
                returnFieldList.add(field);
            }
            else if(accessType.toLowerCase() == 'create' && fieldMap.get(field.trim()).getDescribe().isCreateable()) {
                returnFieldList.add(field);
            }
        }
        return returnFieldList;
    }
    public static Boolean checkFieldAccess(String ObjName , String accessType, List<String> fieldList ) {
        Boolean isTrue = checkObjectAccess(ObjName,accessType);
        String message;
        List<String> restrictedFieldList = new List<String>();
        SObjectType sObjType = Schema.getGlobalDescribe().get(ObjName);
        Map<String , Schema.SObjectField> fieldMap = sObjType.getDescribe().fields.getMap();
        
        for(String field : fieldList){
            if(accessType.toLowerCase() == 'read') {
                System.debug('field-->'+field);
                isTrue = (isTrue && fieldMap.get(field.trim()).getDescribe().isAccessible());
                System.debug('isAccessible-->');
                message = (isTrue) ? '' : System.Label.AG_security_error_field_not_readable;
            }
            if(!isTrue){
                restrictedFieldList.add(ObjName);
                restrictedFieldList.add(field);
                throw new securityException(String.format(message,restrictedFieldList));
            }  
        }
        return isTrue;
    }
    public class securityException extends Exception {} 
}