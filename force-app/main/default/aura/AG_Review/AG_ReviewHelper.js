({
    navigateToReviewDocument : function(component) {
        var event = $A.get("e.force:navigateToComponent");
        event.setParams({
            componentDef : "c:AG_Review_Document",
            componentAttributes: {
                recordId : component.get("v.recordId")
            }
        });
        event.fire();
    }
})