public class AG_Template_Builder_CL {
    public AG_Template_Builder_CL() {}
    
    @AuraEnabled
    public static String getObjectList() {
        try {
            List<Map<String,String>> sObjList = new List<Map<String,String>>();
            for(Schema.SObjectType objTyp : Schema.getGlobalDescribe().Values()) {
                Map<String,String> sObjMap = new Map<String,String>();
                if(objTyp.getDescribe().isAccessible()) {
                    if( !objTyp.getDescribe().getlabel().contains('Change Event')) {
                        String objLabel = objTyp.getDescribe().getlabel();
                        String objName = objTyp.getDescribe().getName();
                        sObjMap.put('label' , objLabel);
                        sObjMap.put('value' , objName);
                        sObjList.add(sObjMap);
                    }
                }   
            } 
            return JSON.serialize(sObjList);
        }
        Catch(Exception e) {
            throw new ApplicationException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static String getChildObjList(String objName) {
        List<Map<String,String>> childObjectMapList = new List<Map<String,String>>();
        
        try {
            SObjectType objType = Schema.getGlobalDescribe().get(objName);
            Schema.DescribeSObjectResult DescribeSObject = objType.getDescribe();
            for(Schema.ChildRelationship childObj: DescribeSObject.getChildRelationships()) {
                Schema.SObjectType childObjDetail = childObj.getChildSObject();
                String childObjName = childObjDetail.getDescribe().getLocalName();
                
                
                if(childObjName.containsIgnoreCase('__c') || childObjName.equalsIgnoreCase('Account') || 
                   childObjName.equalsIgnoreCase('Contact') || childObjName.equalsIgnoreCase('Opportunity') || 
                   childObjName.equalsIgnoreCase('Quote') || childObjName.equalsIgnoreCase('QuoteLineItem') || 
                   childObjName.equalsIgnoreCase('Product') || childObjName.equalsIgnoreCase('Order') || 
                   childObjName.equalsIgnoreCase('OrderItem') || childObjName.equalsIgnoreCase('Case') || 
                   childObjName.equalsIgnoreCase('Solution') || childObjName.equalsIgnoreCase('Campaign') || 
                   childObjName.equalsIgnoreCase('Lead') || childObjName.equalsIgnoreCase('Contract') || 
                   childObjName.equalsIgnoreCase('OpportunityLineItems') || childObjName.equalsIgnoreCase('Asset')
                  ) {
                      Map<String,String> childObjectMap = new Map<String,String>();
                      system.debug('childObjDetail-->'+objName+','+childObj+','+childObj.getRelationshipName());
                      childObjectMap.put('label', childObjDetail.getDescribe().getlabel());
                      childObjectMap.put('api', childObjDetail.getDescribe().getLocalName());
                      childObjectMap.put('value', childObj.getRelationshipName());
                      childObjectMapList.add(childObjectMap);
                  }
            } 
            return JSON.serialize(childObjectMapList);
        }
        Catch(Exception e) {
            throw new ApplicationException(e.getMessage());
        } 
    }
    
    @AuraEnabled
    public static String getParentObjFieldList(String objName) {
        List<objectFieldsWrapper> objectFieldsWrapperList = new List<objectFieldsWrapper>();
        
        Map<String, Schema.SObjectField> fieldMap = new Map<String, Schema.SObjectField>();
        try {
            SObjectType objType = Schema.getGlobalDescribe().get(objName);
            fieldMap = objType.getDescribe().fields.getMap();
            objectFieldsWrapper objWrapper;
            for(String field : fieldMap.keySet()) {
                if((String.valueOf(fieldMap.get(field).getDescribe().getType()) == 'REFERENCE') && !(fieldMap.get(field).getDescribe().isCustom())) {
                    objWrapper = new objectFieldsWrapper(String.valueOf(fieldMap.get(field).getDescribe().getRelationshipName()), String.valueOf(fieldMap.get(field)), String.valueOf(fieldMap.get(field).getDescribe().getType()));
                }
                else {
                    objWrapper = new objectFieldsWrapper(String.valueOf(fieldMap.get(field).getDescribe().getLabel()), String.valueOf(fieldMap.get(field)), String.valueOf(fieldMap.get(field).getDescribe().getType()));
                }
                objectFieldsWrapperList.add(objWrapper);
                
            } 
            system.debug('objectFieldsWrapperList -->' +JSON.serialize(objectFieldsWrapperList));
            return JSON.serialize(objectFieldsWrapperList);
        }
        Catch(Exception e) { 
            throw new ApplicationException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static String getLookupFieldsList(String objName,String fieldName) {
        try {
            String objectName;
            system.debug('objectName-->'+objName);
            system.debug('fieldName-->'+fieldName);
            Schema.DescribeFieldResult field = Schema.getGlobalDescribe()
                .get(objName)
                .getDescribe()
                .fields
                .getMap()
                .get(fieldName)
                .getDescribe();
            
            for(Schema.SObjectType reference : field.getReferenceTo()) {
                objectName = reference.getDescribe().getName();
            }
            
            List<lookupObjectFieldsWrapper> lookupObjectFieldsWrapperList = new List<lookupObjectFieldsWrapper>();
            
            Map<String, Schema.SObjectField> fieldMap = new Map<String, Schema.SObjectField>();
            
            SObjectType objType = Schema.getGlobalDescribe().get(objectName);
            fieldMap = objType.getDescribe().fields.getMap();
            for(String fld : fieldMap.keySet()) {
                lookupObjectFieldsWrapper objWrapper;
                if((String.valueOf(fieldMap.get(fld).getDescribe().getType()) == 'REFERENCE') && !(fieldMap.get(fld).getDescribe().isCustom())) {
                    objWrapper = new lookupObjectFieldsWrapper(String.valueOf(fieldMap.get(fld).getDescribe().getRelationshipName()), String.valueOf(fieldMap.get(fld)), String.valueOf(fieldMap.get(fld).getDescribe().getType()), objectName);
                }
                else {
                    objWrapper = new lookupObjectFieldsWrapper(String.valueOf(fieldMap.get(fld).getDescribe().getLabel()), String.valueOf(fieldMap.get(fld)), String.valueOf(fieldMap.get(fld).getDescribe().getType()), objectName);
                }
                lookupObjectFieldsWrapperList.add(objWrapper);
                
                // lookupObjectFieldsWrapper objWrapper = new lookupObjectFieldsWrapper(String.valueOf(fieldMap.get(fld).getDescribe().getLabel()), String.valueOf(fieldMap.get(fld)), String.valueOf(fieldMap.get(fld).getDescribe().getType()), objectName);
                // lookupObjectFieldsWrapperList.add(objWrapper);
            }
            return JSON.serialize(lookupObjectFieldsWrapperList);
        }
        Catch(Exception e) {
            System.debug('Error message -->' +e.getMessage());
            System.debug('Error line number -->' +e.getLineNumber());
            System.debug('Cause -->' +e.getCause());
            throw new ApplicationException(e.getMessage());
        }
    }
    
    
    Public class objectFieldsWrapper {
        public String Label;
        public String Name;
        public String Type;
        Public objectFieldsWrapper(String Label, String Name, String Type) {
            this.Label = Label;
            this.Name = Name;
            this.Type = Type;
        }
    }
    
    Public class lookupObjectFieldsWrapper {
        public String Label;
        public String Name;
        public String Type;
        public String objName;
        Public lookupObjectFieldsWrapper(String Label, String Name, String Type, String objName) {
            this.Label = Label;
            this.Name = Name;
            this.Type = Type;
            this.objName = objName;
        }
   
    }
     public class ApplicationException extends Exception {}
}