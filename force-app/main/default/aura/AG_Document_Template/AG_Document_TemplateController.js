({
	doInit : function(component, event, helper) {
		var action = component.get("c.getObjectListMap");
        action.setCallback(this,function(response) {
            var state = response.getState();
            if(state == "SUCCESS"){
                var objectList = JSON.parse(response.getReturnValue());
                component.set("v.objList" , objectList);
            }
        });
        $A.enqueueAction(action);
	},
    selectedObject : function(component, event, helper){
        var apiName = component.find("select").get("v.value");
        var objList = component.get("v.objList");
        objList.forEach(function(x) {
                    if(x.value == apiName) {
                       component.set("v.templateObject.AG_Object_Name__c" , x.label);
                    } 
                });        
        component.set("v.templateObject.Object_API_Name__c" , apiName);
     
    },
    saveTemplate : function(component, event, helper){
        var action = component.get("c.saveTemplateObject");
        var templateObj = component.get("v.templateObject");
        action.setParams ({ "templateObj" : JSON.stringify(templateObj) });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if(state == "SUCCESS"){
                var templateObj = JSON.parse(response.getReturnValue());
                var evt = $A.get("e.force:navigateToComponent");
                evt.setParams({
                    componentDef : "c:AG_Object_Template",
                    componentAttributes: {
                        templateObject : templateObj
                        
                    }
                });
                evt.fire();
            }else{
                let errors = response.getError();
                let message = 'Unknown error'
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                console.error(message);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Duplicate Template Found. Use Existing Template."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    navToListView:function(component, event, helper){
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "AG_Template__c"
        });
        homeEvent.fire();
    }
})