({
    getAgreementDetails : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        let action = component.get("c.findAgreement");
        action.setParams({"recordId" : component.get("v.recordId")});
        action.setCallback(this,function(response) {
            if(response.getState() == 'SUCCESS') {
                let res = JSON.parse(response.getReturnValue());
                if(res.success){
                    component.set("v.agreement", res.data);
                    this.changeStatusPath(component);
                }
            }
            else {
                toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "oops!! Something went wrong" });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    changeStatusPath : function(component) {
        let agreementRecord = component.get("v.agreement");
        let status = agreementRecord.AG_Status__c;
        let currentStatus = '';
        
        if(status == 'Requested'){
            currentStatus = 'request';
        }
        else if(status == 'In Review'){
            currentStatus = 'review';
        }
            else if(status == 'In Approval'){
                currentStatus = 'approval';
            }
                else if(status == 'Approved' || status == 'In Signature'){
                    currentStatus = 'signature';
                }
                    else if(status == 'Activated'){
                        currentStatus = 'AllDone';
                    }
        component.set("v.pathValue", currentStatus);
    }
})