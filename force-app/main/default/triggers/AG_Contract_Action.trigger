trigger AG_Contract_Action on AG_Agreement__c ( after insert, after update) {
    AG_Contract_Actions_CL.contractActions(Trigger.new);
}