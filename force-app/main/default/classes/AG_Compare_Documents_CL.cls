public class AG_Compare_Documents_CL {
    
    @AuraEnabled
    public static String getDocuments(Id recordId){
        try{
            List<ContentVersion> contentVersionList = [SELECT Id,Title,IsLatest,FileExtension,ContentDocumentId,CreatedBy.Name,CreatedDate FROM ContentVersion WHERE FirstPublishLocationId =: recordId AND IsLatest =: true];
            AG_Agreement__c  agreementRecord = [SELECT Id,Name FROM AG_Agreement__c WHERE Id =:  recordId];
            String sObjName = recordId.getSObjectType().getDescribe().getLabel();  
        	System.debug('Object Type is ' + sObjName);
            if(contentVersionList.size() > 0){
                return '{"success": true, "data": '+JSON.serialize(contentVersionList)+', "agreement":'+JSON.serialize(agreementRecord)+',"objLabel":'+JSON.serialize(sObjName)+'}';
            }
            else{
                return '{"success": false, "message" : " No document(s) found "}';
            }
        }
        catch(Exception e){
            System.debug(e.getLineNumber());
            System.debug(e.getMessage());
        }
        return '';
    }
    
    /*
    @isTest static void callGetDocuments(){
        User contractManageruser = AG_Document_Test_Data.createTestUser();
        System.runAs(contractManageruser) {
            Account account = AG_Document_Test_Data.createAccount();
            Contact contact = AG_Document_Test_Data.createContact(account);
            AG_Agreement_Days_Price__c agreementDaysPrice = AG_Document_Test_Data.createAgreementDaysPrice();
            AG_Agreement__c agreement = AG_Document_Test_Data.createAgreement(account, agreementDaysPrice);
            insert agreement;
            List<ContentVersion> contentVersionList = [SELECT Id,Title,IsLatest,FileExtension,ContentDocumentId,CreatedBy.Name,CreatedDate FROM ContentVersion WHERE FirstPublishLocationId =: agreement.Id AND IsLatest =: true];
            String expectedResult = '{"success": false, "message" : " No document(s) found "}';
            String actualResult =  AG_Document_CL.getDocuments(agreement.Id);
            System.assertEquals(expectedResult,actualResult);
        }
    }*/
    
    @AuraEnabled
    public static String compareDocuments(Id recordId, String documentOne, String documentTwo){
        try{
            Http http = new Http();
            Httprequest request = new Httprequest();
            
            ContentVersion leftContentFile = (ContentVersion) System.JSON.deserialize(documentOne, ContentVersion.class);
            ContentVersion rightContentFile = (ContentVersion) System.JSON.deserialize(documentTwo, ContentVersion.class);
            
            ContentVersion leftFile = [SELECT Id, Title, VersionData, FileExtension FROM ContentVersion WHERE ContentDocumentId =: leftContentFile.ContentDocumentId];
            ContentVersion rightFile = [SELECT Id, Title, VersionData, FileExtension FROM ContentVersion WHERE ContentDocumentId =: rightContentFile.ContentDocumentId];
            System.debug('FileExtension-->'+rightContentFile.FileExtension);
            System.debug('FileExtension-->'+leftFile.FileExtension);
            String leftFormData = '';
            String rightFormData = '';
            String publicParam = '';
            String authToken = 'Token ef01a3e3ccfdb7b8d7e5f09f157210f1';
            String contentType = AG_Form_Multipart_CL.GetContentType();
            
            leftFormData += AG_Form_Multipart_CL.append('left.file_type', leftFile.FileExtension );
            rightFormData += AG_Form_Multipart_CL.append('right.file_type',rightFile.FileExtension );
            publicParam += AG_Form_Multipart_CL.append('public','true');
            Blob publicParamBlob = AG_Form_Multipart_CL.makeBlob(publicParam);
            System.debug('leftFormData-->' +String.escapeSingleQuotes('pdf'));
            
            Blob leftFileBlob = AG_Form_Multipart_CL.makeBlobWithFile('left.file', leftFile.VersionData, leftFile.Title, leftFormData);
            Blob rightFileBlob= AG_Form_Multipart_CL.makeBlobWithFile('right.file', rightFile.VersionData, rightFile.Title, rightFormData);
            
             
            String left = EncodingUtil.base64Encode(leftFile.VersionData);
            String right = EncodingUtil.base64Encode(rightFile.VersionData);
            
            String combinedDataAsHex = EncodingUtil.convertToHex(leftFileBlob) + EncodingUtil.convertToHex(rightFileBlob) + EncodingUtil.convertToHex(publicParamBlob) ;
            Blob blobBodyFile = EncodingUtil.convertFromHex(combinedDataAsHex);
            String leftFileType = leftFile.FileExtension;
            String rightFileType = rightFile.FileExtension;
        //  String leftds = leftFile.VersionData.toString();
        //  String lefdstds = rightFile.VersionData.toString();

            String bodyString = '{"leftFile": "'+ left +'", "rightFile": "'+ right+ '","leftFieldType": "'+ leftFileType +'","rightFieldType": "'+ rightFileType +'"}';
            
        //  System.debug('blobBodyFile-->'+blobBodyFile.toString());
            request.setHeader('Authorization',authToken);
            request.setHeader('Content-Type',contentType);
            request.setHeader('Accept','application/json');
            request.setBodyAsBlob(blobBodyFile);

          // request.setBody(bodyString);
            
            request.setMethod('POST');
            request.setEndpoint('https://api.draftable.com/v1/comparisons');
         // request.setEndpoint('http://ec2-13-233-250-208.ap-south-1.compute.amazonaws.com:8080/api/upload');
         // request.setEndpoint('https://document-comparison.herokuapp.com/api/upload');
            request.setHeader('Content-Length',String.valueof(request.getBodyAsBlob().size()));
            
            HTTPResponse response = http.send(request);
             System.debug('response-->'+response.getBody());
            if (response.getStatusCode() == 201) {
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                String identifierValue = (String) results.get('identifier');
                String urlValue = 'https://api.draftable.com/v1/comparisons/viewer/'+'EfFwhg-test'+'/'+identifierValue;
                System.debug('response-->'+urlValue);
                 return '{"success": true, "data": '+JSON.serialize(urlValue)+'}';
            }else{
                 return '{"success": false, "message": "Something Went Wrong!"}';
            }
            
        }
        catch(Exception e){
            System.debug('getLineNo-->' +e.getLineNumber());
            System.debug('getMessage-->' +e.getMessage());
            return '{"success": false, "message": '+e.getMessage()+'}';
        }
     }
    
    public static String createSignature(String accountId, String authToken, String identifier, Integer validUntilTimeStamp){
      
        String jsonString = '{"account_id": "'+accountId+'", "identifier": "'+identifier+'", "valid_until": '+validUntilTimeStamp+'}';
        System.debug('jsonString-->' +jsonString);
         
        String algorithmName = 'hmacSHA256';
        String secretKey = EncodingUtil.urlencode(authToken, 'UTF-8');
        System.debug('secretKey-->' +secretKey);
        
        String encodedJson = EncodingUtil.urlencode(jsonString, 'UTF-8');
        System.debug('encodedJson-->' +encodedJson);
        
       /* Blob json = Blob.valueOf(encodedJson);
        
        String convertedJsonHex = EncodingUtil.convertToHex(json);
        System.debug('convertedJsonHex-->' +convertedJsonHex);
        
        Blob digestHex = Blob.valueOf(convertedJsonHex);
        
        Blob digestValue = Crypto.generateDigest('SHA-256', digestHex);
        
        String result = EncodingUtil.convertToHex(digestValue);
        System.debug('result-->' +result);*/
        
        Blob digestHex = Blob.valueOf(encodedJson);
        Blob digestValue = Crypto.generateDigest('SHA-256', digestHex);
        String result = EncodingUtil.convertToHex(digestValue);
        
        Blob hmacData = Crypto.generateMac(algorithmName, Blob.valueOf(result), Blob.valueOf(secretKey));
        System.debug('hmacData-->' +hmacData);
        
        String signature = EncodingUtil.convertToHex(hmacData); //7052407
        System.debug('signature-->' +signature);
        
        String contentUrl = 'https://api.draftable.com/v1/comparisons/viewer/kwfKrQ-test/'+identifier+'?valid_until='+validUntilTimeStamp+'&signature='+signature;
        System.debug('contentUrl-->' +contentUrl);
        
        return contentUrl;
    }
      
}