({
    doInit : function(component, event, helper) {
        component.set("v.toggleSpinner", true);
        var recordId = component.get("v.recordId");
        var toastEvent = $A.get("e.force:showToast");
        var action = component.get("c.checkBackProcessIsValid");
        action.setParams({
            "recordId" : component.get("v.recordId"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.toggleSpinner" , false);
            if(state == 'SUCCESS'){
                var res = JSON.parse(response.getReturnValue());
                if(res.success){
                    component.set("v.hasNoError" , res.data);  
                }
                else{
                    component.set("v.hasNoError" , false);
                    toastEvent.setParams({ "title": "Error!", "type": "Error", "message": res.message });
                    toastEvent.fire();
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": component.get("v.recordId"),
                        "slideDevName": "details"
                    });
                    navEvt.fire();
                }
            }
            else{
                toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "Something went wrong" });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    navigateToParent : function(component,event,helper) {
        helper.navigate(component);
    },
    
    closeModal : function(component,event,helper) {
        component.set("v.isModal" , false);
    },
    
    proceedAction : function(component,event,helper){
        var selectedValue = component.get("v.selectedBackValue");
        if(selectedValue == '' || selectedValue == null || selectedValue == undefined){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "Please choose an action to proceed" });
            toastEvent.fire();
        }
        else{
            component.set("v.isModal" , true);
        }
    },
    
    callSave : function(component,event,helper){
        var toastEvent = $A.get("e.force:showToast");
        var commentedValue = component.get("v.commentedValue");
        if(commentedValue == '' || commentedValue == null || commentedValue == undefined){
            toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "Please enter comments" });
            toastEvent.fire();
        }
        else{
            component.set("v.toggleSpinner" , true);
            var action = component.get("c.performBackProcess");
            action.setParams({
                "recordId" : component.get("v.recordId"),
                "selectedValue" : component.get("v.selectedBackValue"),
                "commentedValue": commentedValue
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                component.set("v.toggleSpinner" , false);
                if(state == 'SUCCESS'){
                    var res = JSON.parse(response.getReturnValue());
                    if(res.success){
                        toastEvent.setParams({ "title": "Success!", "type": "Success", "message": res.message });
                        toastEvent.fire();
                        helper.navigate(component);
                    }
                    else{
                        toastEvent.setParams({ "title": "Error!", "type": "Error", "message": res.message });
                        toastEvent.fire();
                    }
                }
                else{
                    toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "Something went wrong" });
                    toastEvent.fire();
                }
            }); 
            $A.enqueueAction(action);
        }
    }
})