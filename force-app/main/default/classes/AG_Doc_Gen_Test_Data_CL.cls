public with sharing class AG_Doc_Gen_Test_Data_CL {
    public AG_Doc_Gen_Test_Data_CL(){}
    public static User createUser() { 
        List<String> profileFieldList = new List<String>{'Id'};
            Profile profile = null ;
        if(AG_Document_Gen_FLS_CL.checkFieldAccess('Profile', 'read', profileFieldList)) {
            profile = [SELECT Id FROM Profile WHERE Name='Standard User'];
        }
        List<String> userFieldList = new List<String>{'Id','Alias','Email','EmailEncodingKey','LastName','LanguageLocaleKey','TimeZoneSidKey','LocaleSidKey','ProfileId','UserName'};
            User user = new User(Alias = 'Ranjith', Email='ranjithraja@aspigrow.com',EmailEncodingKey ='UTF-8',
                                 LastName='Raja',LanguageLocaleKey='en_US',TimeZoneSidKey = 'America/Los_Angeles',
                                 LocaleSidKey='en_US', ProfileId = profile.Id,
                                 UserName='ranjith_2696@gmail.com');
        if(AG_Document_Gen_FLS_CL.checkFieldAccess('User','create',userFieldList)) {
            insert user;
        }
        return user;
    }
    public static void createPermissionSetForAdmin(String userId) {
        List<String> permissionSetFields = new List<String>{'Id','Name'};
        PermissionSet permissionSet = null ;
        if(AG_Document_Gen_FLS_CL.checkFieldAccess('PermissionSet', 'read', permissionSetFields)) {
            permissionSet = [SELECT Id , Name FROM PermissionSet WHERE Name = 'AG_Doc_Gen'];
        }
        PermissionSetAssignment assignment = new PermissionSetAssignment();
        assignment.AssigneeId = userId;
        assignment.PermissionSetId = permissionSet.Id;
        List<String> permissionSetAssignmentFields = new List<String>{'PermissionSetId','AssigneeId'};
            if(AG_Document_Gen_FLS_CL.checkFieldAccess('PermissionSetAssignment','create', permissionSetAssignmentFields)) {
                insert assignment;
            }
    }
    public static string createTemplate(){
        List<String> templateFieldList = new List<String>{'AG_Document_Type__c','AG_isActive__c','Object_API_Name__c','AG_Object_Name__c'};
        AG_Template__c templateRecord = null ;
        if(AG_Document_Gen_FLS_CL.checkFieldAccess('AG_Template__c','read', templateFieldList)) {
            templateRecord = new AG_Template__c(AG_Document_Type__c='.DOC',AG_isActive__c =true,Object_API_Name__c='Account',AG_Object_Name__c='Account');
        }
        return JSON.serialize(templateRecord);
    }
    public static string createAccount(){
        List<String> accountFieldList = new List<String>{'Name','AccountNumber','BillingCountry'};
        Account newAccount = null;
        newAccount = new Account(Name='Raja', AccountNumber='2544646', BillingCountry='India');
        if(AG_Document_Gen_FLS_CL.checkFieldAccess('Account','create', accountFieldList)) {
            insert newAccount;
        }
        return newAccount.Id;
    }
    public static string createAccountDataForDocGen(){
    /*    List<String> operatingHoursFieldList = new List<String>{'Name'};
        OperatingHours newOperHour = new OperatingHours(Name='OperHours');
        if(AG_Document_Gen_FLS_CL.checkFieldAccess('OperatingHours','create', operatingHoursFieldList)) {
        insert newOperHour;
        } */
        List<String> accountFieldList = new List<String>{'Name','AccountNumber','BillingCountry'};
        Account newAccount = new Account(Name='Raja', AccountNumber='2544646', BillingCountry='India');
        if(AG_Document_Gen_FLS_CL.checkFieldAccess('Account','create', accountFieldList)) {
        insert newAccount;
        }
        List<String> contactFieldList = new List<String>{'Salutation','Name','AccountId','Email','Title','Department'};
        Contact newContact = new Contact(Salutation='Ms',FirstName='Suru',Lastname='A',AccountId=newAccount.id,Email='suruthi@aspigrow.com',Title='Template',Department='Developer');
        if(AG_Document_Gen_FLS_CL.checkFieldAccess('Contact','create', contactFieldList)) {
        insert newContact; 
        }
        return newAccount.Id;
    }
}