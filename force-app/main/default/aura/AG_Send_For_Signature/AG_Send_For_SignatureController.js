({
    doInit : function(component, event, helper) {
        var body = '';
        var subject = '';
        var modifiedDate = new Date();
        component.set("v.toggleSpinner", true);
        var toastEvent = $A.get("e.force:showToast");
        var action = component.get("c.getContacts");
        action.setParams({
            "recordId" : component.get("v.recordId"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.isUpload", false);
                component.set("v.toggleSpinner" , false);
                var res = JSON.parse(response.getReturnValue());
                if(res.success){
                    component.set("v.hasNoError" , true);
                    component.set("v.isReview" , true);
                    var contactList = res.data;
                    var documentList = res.document;
                    var templateList = res.templateList;
                    modifiedDate = new Date(res.document.CreatedDate).getDate() + '/' + (new Date(res.document.CreatedDate).getMonth() + 1) + '/' + new Date(res.document.CreatedDate).getFullYear();
                    component.set("v.modifiedDate" , modifiedDate);
                    component.set("v.contactList", contactList);
                    component.set("v.documentList", documentList);
                    component.set("v.templateList", templateList);
                }
                else{
                    helper.navigateToParent(component);
                    toastEvent.setParams({ "title": "Error!", "type": "Error", "message": res.message });
                    toastEvent.fire();
                }
            }
            else{
                toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "oops!! Something went wrong" });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    isCheckBoxTrue : function(component, event, helper){
        var isChecked = event.getSource().get('v.checked');
        component.set("v.isChecked", isChecked);
        
        if(component.get("v.isChecked")){
            component.set("v.isShow", false);
        }
        else{
            component.set("v.isShow", true);
        }
    },
    
    getSelectedTemplates : function(component, event, helper) {
        var templateList = component.get("v.templateList");
        for(var x of templateList){
            if(x.Name == component.find("tempName").get("v.value")){
                component.set("v.subject", x.Subject);
                component.set("v.body", x.Body);
                component.set("v.emailId", x.Id);
            }
            else if(component.find("tempName").get("v.value") == '' || component.find("tempName").get("v.value") == 'None'){
                component.set("v.subject", '');
                component.set("v.body", '');
            }
        }
    },
    
    closeModel : function(component, event, helper) {
        var event = $A.get("e.force:navigateToSObject");
        event.setParams({
            "recordId": component.get("v.recordId"),
            "slideDevName": "Details"
        });
        event.fire();
        component.set("v.isReview" , false);
        $A.get("e.force:closeQuickAction").fire();
    },
    
    callNext : function(component, event, helper) {
        var selectedContactList = [];
        var contactList = component.get("v.contactList"); 
        var toAddressList = [];
        for(var contact of contactList){
            if(contact.isSelected){
                toAddressList.push(contact.contacts.Email);
                component.set("v.toAddress", toAddressList);
                selectedContactList.push(contact.contacts);
            }
        }
        if(selectedContactList == null || selectedContactList == '' || selectedContactList == undefined){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "Please select a contact to proceed" });
            toastEvent.fire();
        }
        else{
            component.set("v.selectedContactList" , selectedContactList);
            component.set("v.isReview" , false);
            component.set("v.isNext" , true);
        }
    },
    
    selectAllContacts : function(component, event, helper) {
        var checkedvalue = component.get("v.isSelectAllContacts");
        try {
            checkedvalue = event.getSource().get("v.value");
        }
        catch(e) {}
        var filteredContactList = component.get("v.contactList");
        for(var x in filteredContactList) {
            if(filteredContactList.hasOwnProperty(x)) {
                filteredContactList[x].isSelected = checkedvalue;
            }
        }
        component.set("v.contactList", filteredContactList);
    },
    
    selectAllDocuments : function(component, event, helper) {
        helper.getAllDocuments(component);
    },
    
    proceedNext : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        var selectedDocumentList = [];
        var documentList = component.get("v.documentList");
        for(var document of documentList){
            if(document.isSelected){
                selectedDocumentList.push(document.contentVersion);
                component.set("v.selectedDocumentList",selectedDocumentList);
            }
        }
        if(selectedDocumentList == null || selectedDocumentList == '' || selectedDocumentList == undefined){
            toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "Please select a document to proceed" });
            toastEvent.fire();
        }
        else{
            component.set("v.hasNoError" , false);
            component.set("v.isNext" , false);
            component.set("v.isEmail" , true);
        }
    },
    
    callPrevious : function(component, event, helper) {
        if(component.get("v.isEmail")){
            component.set("v.isEmail" , false);
            component.set("v.isNext" , true);
        }
        else{
            component.set("v.isNext" , false);
            component.set("v.isReview" , true);
        }
    },
    
    callSendForSignature : function(component, event, helper) {
        component.set("v.toggleSpinner" , true);
        var toMailId = component.find("toMail").get("v.value");
        var ccAddress = component.get("v.ccAddress");
        
        var selectedDocumentList = component.get("v.selectedDocumentList");
        var toastEvent = $A.get("e.force:showToast");
        var selectedContactList = component.get("v.selectedContactList");
        var templateList = component.get("v.templateList")
        
        if(toMailId == null || toMailId == ''){
            toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "Please enter to address" });
            toastEvent.fire();
        }
        else if(selectedDocumentList == null || selectedDocumentList == '' || selectedDocumentList == undefined){
            toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "No document is selected" });
            toastEvent.fire();
        }
        else if(selectedContactList == null || selectedContactList == '' || selectedContactList == undefined){
            toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "No contact is selected" });
            toastEvent.fire();
        }
        else{
            var action = component.get("c.sendForSignature");
            action.setParams({
                "recordId" : component.get("v.recordId"),
                "selectedContacts" : JSON.stringify(selectedContactList),
                "selectedDocuments" : JSON.stringify(selectedDocumentList),
                "templates" : JSON.stringify(templateList),
                "toAddress" : JSON.stringify(component.get("v.toAddress")),
                "ccAddress" : JSON.stringify(ccAddress),
                "emailId" : component.get("v.emailId"),
                "subject" : component.get("v.subject"),
                "body" : component.get("v.body")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                component.set("v.toggleSpinner", false);
                if(state === "SUCCESS") {
                    component.set("v.isReview", false);
                    var res = JSON.parse(response.getReturnValue());
                    if(res.success){
                        toastEvent.setParams({ "title": "Success!", "type": "Success", "message": res.message });
                        toastEvent.fire();
                        helper.navigateToParent(component);
                    }
                    else{
                        toastEvent.setParams({ "title": "Error!", "type": "Error", "message": res.message });
                        toastEvent.fire();
                    }
                }
                else{
                    toastEvent.setParams({ "title": "Error!", "type": "Error", "message": "oops!! something went wrong" });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
    }
})