@isTest
public class AG_Form_Multipart_Test_CL {
    @isTest static void GetContentTypeTest() {
        Test.startTest();
        String objList = AG_Form_Multipart_CL.GetContentType();
        System.assertEquals(objList,objList);
        System.assertNotEquals(objList,null);
        Test.stopTest();
    }
    @isTest static void SafelyPadTest() {
        String objList = AG_Form_Multipart_CL.SafelyPad('Test','Test','Test');
    }
    @isTest static void WriteBoundaryTest() {
        Test.startTest();
        String objList = AG_Form_Multipart_CL.WriteBoundary();
        Test.stopTest();
    }
    @isTest static void WriteBoundaryTestwithParams() {
        EndingType endType = null;
        String objList = AG_Form_Multipart_CL.WriteBoundary(null);
       
    }
    @isTest static void WriteBodyParameterTest() {
        String objList = AG_Form_Multipart_CL.WriteBodyParameter('TestFile','Test');
    }
    @isTest static void writeFileBodyTest() {
        Blob body = Blob.valueof('Some random String');
        String objList = AG_Form_Multipart_CL.writeFileBody('TestFile',body,'Test');
    }
    @isTest static void makeBlobWithFileTest() {
        Blob body = Blob.valueof('Some random String');
        Blob multiPartForm = AG_Form_Multipart_CL.makeBlobWithFile('File',body,'document','Test====');
    }
    @isTest static void makeBlobTest() {
        Blob body = Blob.valueof('Some random String');
        Blob objList = AG_Form_Multipart_CL.makeBlob('TestFile');
    }
    @isTest static void appendTest() {
        String objList = AG_Form_Multipart_CL.append('TestFile','Test');
    }
     public enum EndingType {
        Cr,
            CrLf,
            None
            }
}