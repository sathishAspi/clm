@isTest
global class AG_DocGen_MockResponse_Test_CL implements HttpCalloutMock {
// Implement this interface method
global static HTTPResponse  respond(HTTPRequest req) {
  // Optionally, only send a mock response for a specific endpoint
  // and method.
  System.assertEquals('https://documeta-gen.herokuapp.com/handle_form','https://documeta-gen.herokuapp.com/handle_form');
  System.assertEquals('POST', req.getMethod());

  // Create a fake response
  HttpResponse res = new HttpResponse();
  res.setHeader('Content-Type', 'application/json');
  String recordId = AG_Doc_Gen_Test_Data_CL.createAccount();
  res.setBody('{"jsonData":"test","recordId":"'+recordId+'","fileName":"test","folderId":"rewrtwert112132131"}');
  res.setStatusCode(200);
  return res;
}
}