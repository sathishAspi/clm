({
    navigateToCompareDocument : function(component) {
        var event = $A.get("e.force:navigateToComponent");
        event.setParams({
            componentDef : "c:AG_Compare_Documents",
            componentAttributes: {
                recordId : component.get("v.recordId")
            }
        });
        event.fire();
    }
})