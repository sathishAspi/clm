@isTest
public with sharing class AG_Document_Test_Data {
    
    public static User createTestUser(){
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User user = new User(Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',
                             LastName='Testing',LanguageLocaleKey='en_US',TimeZoneSidKey = 'America/Los_Angeles',
                             LocaleSidKey='en_US', ProfileId = profile.Id,
                             UserName='prabunagul@aspigrow.com'); 
        insert user;
        return user;
    }
    
     public static void createPermissionSetForAdmin(String userId) {
        PermissionSet permissionSet = [SELECT Id , Name FROM PermissionSet WHERE Name = 'AG_CLM_Admin'];
        PermissionSetAssignment assignment = new PermissionSetAssignment();
        assignment.AssigneeId = userId;
        assignment.PermissionSetId = permissionSet.Id;
        insert assignment;
    }
    
    public static Account createAccount(){
        Account account = new Account();
        account.Name = 'Test Account';
        insert account;
        return account;
    }
    
    public static Contact createContact(Account account){
        Contact contact = new Contact();
        contact.LastName = 'Test';
        contact.Email = 'test@testOrg.com';
        contact.AccountId = account.Id;
        insert contact;
        return contact;
    }
    
    public static AG_Agreement_Days_Price__c createAgreementDaysPrice(){
        AG_Agreement_Days_Price__c agreementDaysPrice = new AG_Agreement_Days_Price__c();
        agreementDaysPrice.AG_Request_To_Review_Price__c = 20;
        agreementDaysPrice.AG_Review_To_Approval_Price__c = 20;
        agreementDaysPrice.AG_Approval_To_Signature_Price__c = 20;
        agreementDaysPrice.AG_Signature_To_Activation_Price__c = 20;
        insert agreementDaysPrice;
        return agreementDaysPrice;
    }
    
    public static AG_Agreement__c createAgreement(Account account, AG_Agreement_Days_Price__c agreementDaysPrice){
        AG_Agreement__c agreement = new AG_Agreement__c();
        agreement.AG_Account__c = account.Id;
        agreement.AG_Agreement_Days_Price__c = agreementDaysPrice.Id;
        agreement.AG_Agreement_Name__c = 'Test agreement';
        agreement.AG_Agreement_Value__c = 500;
        agreement.AG_Start_Date__c = System.today();
        agreement.AG_End_Date__c = System.today()+20;
        agreement.AG_Status__c = 'Requested';
        return agreement;
    }
    
    public static ContentVersion createContentVersion(){
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.Title = 'Test Document';
        contentVersion.PathOnClient = 'TestDocument.pdf';
        contentVersion.VersionData = Blob.valueOf('Test Content');
        contentVersion.IsMajorVersion = true;
        insert contentVersion;
        return contentVersion;
    }
    
    public static ContentDocumentLink createContentDocLink(AG_Agreement__c agreement){
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        ContentDocumentLink contentDoclink = new ContentDocumentLink();
        contentDoclink.LinkedEntityId= agreement.Id;
        contentDoclink.ContentDocumentId= documents[0].Id;
        contentDoclink.Visibility = 'AllUsers'; 
        insert contentDoclink;
        return contentDoclink;
    }
}