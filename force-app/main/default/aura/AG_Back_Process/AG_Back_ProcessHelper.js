({
    navigate : function(component) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId"), 
            "slideDevName": "details"
        });
        navEvt.fire();
    }
})