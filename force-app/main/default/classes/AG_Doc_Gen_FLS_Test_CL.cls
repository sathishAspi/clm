@isTest
public class AG_Doc_Gen_FLS_Test_CL {

    @isTest static void callCheckObjectAccess() {
        Test.startTest();
        User StandardUser = AG_Doc_Gen_Test_Data_CL.createUser();
        AG_Doc_Gen_Test_Data_CL.createPermissionSetForAdmin(StandardUser.Id);
        
        System.runAs(StandardUser){
            AG_Document_Gen_FLS_CL.checkObjectAccess('Account', 'read');
            System.assertEquals(true, AG_Document_Gen_FLS_CL.checkObjectAccess('Account', 'read'));
        }
        Test.stopTest();
    }
    
    @isTest static void callGetFieldsWithAccess() {
        Test.startTest();
        List<String> fieldList = new List<String>();
        fieldList.add('Name');
        fieldList.add('Description');
        fieldList.add('Phone');
        User StandardUser = AG_Doc_Gen_Test_Data_CL.createUser();
        AG_Doc_Gen_Test_Data_CL.createPermissionSetForAdmin(StandardUser.Id);
        
        System.runAs(StandardUser){
            AG_Document_Gen_FLS_CL.getFieldsWithAccess('Account', 'read', fieldList);
            System.assertEquals(true, AG_Document_Gen_FLS_CL.getFieldsWithAccess('Account', 'read', fieldList).size() > 0);
        }
    }
    
    @isTest static void callCheckFieldAccess() {
        Test.startTest();
        List<String> fieldList = new List<String>();
        fieldList.add('Name');
        fieldList.add('Description');
        fieldList.add('Phone');
        fieldList.add('Type');
        User StandardUser = AG_Doc_Gen_Test_Data_CL.createUser();
        AG_Doc_Gen_Test_Data_CL.createPermissionSetForAdmin(StandardUser.Id);
        
        System.runAs(StandardUser){
            AG_Document_Gen_FLS_CL.checkFieldAccess('Account', 'read', fieldList);
            System.assertEquals(true, AG_Document_Gen_FLS_CL.checkFieldAccess('Account', 'read', fieldList));
        }
    }

}