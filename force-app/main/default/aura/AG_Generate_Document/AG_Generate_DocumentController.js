({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:AG_Choose_Template",
            componentAttributes: {
                recordId : recordId
            }
        });
        evt.fire();
    }
})