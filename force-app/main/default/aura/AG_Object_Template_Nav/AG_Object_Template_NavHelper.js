({
    navToTemplateBuilder : function(component) {
        var event = $A.get("e.force:navigateToComponent");
        event.setParams({
            componentDef : "c:AG_Object_Template"
        });
        event.fire();
    }
})