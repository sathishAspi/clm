public with sharing class AG_Contract_Actions_CL {
    
    public static List<AG_Agreement_Days__c> existingAgreementDaysList = new List<AG_Agreement_Days__c>();
    public static List<AG_Agreement_Days__c> agreementDaysList = new List<AG_Agreement_Days__c>();
    
    public static void contractActions(List<AG_Agreement__c> agreementList){
        if(Trigger.isAfter && Trigger.isInsert){
            for(AG_Agreement__c agreement : agreementList){
                if(agreement.AG_Status__c == 'Requested'){
                    AG_Agreement_Days__c agreementDays = new AG_Agreement_Days__c();
                    agreementDays.AG_Agreement__c = agreement.Id;
                    agreementDays.AG_Agreement_Stage__c = agreement.AG_Status__c; 
                    agreementDays.AG_Start_Date__c = agreement.AG_Start_Date__c;
                    agreementDaysList.add(agreementDays);
                }
            }
            List<String> fieldListToInsert = new List<String>{'AG_Agreement_Stage__c', 'AG_Start_Date__c'};
                if(
                    AG_FLS_CL.checkFieldAccess('AG_Agreement_Days__c', 'insert', fieldListToInsert)
                ){
                    insert agreementDaysList;    
                }
        }
        
        if(Trigger.isAfter && Trigger.isUpdate){
            List<AG_Agreement_Days__c> agreementDaysListDesc = [SELECT Id,AG_Agreement__c,AG_Agreement_Stage__c,AG_Start_Date__c,AG_End_Date__c FROM AG_Agreement_Days__c WHERE AG_Agreement__c IN : agreementList ORDER By Createddate DESC LIMIT 1];
            for(AG_Agreement__c agreement : agreementList){
                for(AG_Agreement_Days__c agreementDaysDesc : agreementDaysListDesc){
                    if(agreement.AG_Status__c == 'In Review'){
                        createAgreementDays(agreement, agreementDaysDesc); 
                    }
                    else if(agreement.AG_Status__c == 'In Approval'){
                        createAgreementDays(agreement, agreementDaysDesc); 
                    }
                    else if(agreement.AG_Status__c == 'In Signature'){
                        createAgreementDays(agreement, agreementDaysDesc); 
                    }
                    else if(agreement.AG_Status__c == 'Activated'){
                        createAgreementDays(agreement, agreementDaysDesc); 
                    }
                }
            }
            List<String> fieldListToUpdate = new List<String>{'AG_End_Date__c'};
                if(existingAgreementDaysList.size() > 0){
                    if(
                        AG_FLS_CL.checkFieldAccess('AG_Agreement_Days__c', 'update', fieldListToUpdate)
                    ){
                        update existingAgreementDaysList;
                    }
                }
            
            List<String> fieldListToUpsert = new List<String>{'AG_Agreement_Stage__c','AG_Start_Date__c','AG_End_Date__c'};
                if(agreementDaysList.size() > 0){
                    if(
                        AG_FLS_CL.checkFieldAccess('AG_Agreement_Days__c', 'insert', fieldListToUpsert)
                        &&
                        AG_FLS_CL.checkFieldAccess('AG_Agreement_Days__c', 'update', fieldListToUpsert)
                    ){
                        upsert agreementDaysList;
                    }
                }
        }
    }
    
    private static List<AG_Agreement_Days__c> createAgreementDays(AG_Agreement__c agreement, AG_Agreement_Days__c agreementDaysDesc){
        AG_Agreement_Days__c agreementDays = new AG_Agreement_Days__c(); 
        agreementDays.AG_Agreement__c = agreementDaysDesc.AG_Agreement__c;
        agreementDays.AG_Agreement_Stage__c = agreement.AG_Status__c; 
        agreementDays.AG_Start_Date__c = System.today();
        if(agreement.AG_Is_Changed__c){
            agreementDays.AG_End_Date__c = System.today(); 
        }
        else if(agreement.AG_Status__c == 'Activated' && !agreement.AG_Is_Changed__c){
            agreementDays.AG_End_Date__c = System.today(); 
        }
        agreementDaysDesc.AG_End_Date__c = System.today();
        existingAgreementDaysList.add(agreementDaysDesc);
        agreementDaysList.add(agreementDays);
        
        if(existingAgreementDaysList.size() > 0){
            return existingAgreementDaysList;
        }
        if(agreementDaysList.size() > 0){
            return agreementDaysList;
        }
        return null;
    }
}