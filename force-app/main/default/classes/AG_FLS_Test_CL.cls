@isTest
public class AG_FLS_Test_CL {
    
    @isTest static void callFieldsReadAccess(){
        List<String> agreementList = new List<String>{'Id','Name'};
            List<String> actualResult = AG_FLS_CL.getFieldsWithAccess('AG_Agreement__c', 'read', agreementList);
        System.assertEquals(actualResult.contains('Id'),true);
    }
    @isTest static void callFieldsCreateAccess(){
        List<String> agreementList = new List<String>{'AG_Status__c','Name'};
            List<String> actualResult = AG_FLS_CL.getFieldsWithAccess('AG_Agreement__c', 'create', agreementList);
        System.assertEquals(actualResult.contains('Id'), false);
    }
    @isTest static void callFieldsUpdateAccess(){
        List<String> agreementList = new List<String>{'AG_Status__c'};
            List<String> actualResult = AG_FLS_CL.getFieldsWithAccess('AG_Agreement__c', 'Update', agreementList);
        System.assertEquals(actualResult.contains('Id'), false);
    }
}