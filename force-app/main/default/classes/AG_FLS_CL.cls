public with sharing class AG_FLS_CL {
    
    public static Boolean checkObjectAccess(String objName , String accessType) {
        String message;
        List<String> objList = new List<String>();
        Boolean isTrue = true;
        SObjectType sObjType = Schema.getGlobalDescribe().get(ObjName);
        if(accessType.toLowerCase() == 'read') {
            isTrue = (isTrue && sObjType.getDescribe().isAccessible());
            message = (isTrue) ? '' : 'You do not have permission to read {0}';
        }
        else if(accessType.toLowerCase() == 'create') {
            
            isTrue = (isTrue && sObjType.getDescribe().isCreateable());
            message = (isTrue) ? '' : 'You do not have permission to Create {0}';
        }
        else if(accessType.toLowerCase() == 'update') {
            isTrue = (isTrue && sObjType.getDescribe().isUpdateable());
            message = (isTrue) ? '' : 'You do not have permission to update {0}';
            
        }
        else if(accessType.toLowerCase() == 'delete') {
            isTrue = (isTrue && sObjType.getDescribe().isDeletable());
            message = (isTrue) ? '' : 'You do not have permission to delete {0}';
        }
        objList.add(ObjName);
        if(!isTrue){
            throw new securityException(String.format(message, objList)) ;
        }
        return isTrue; 
    }
    
    public static List<String> getFieldsWithAccess( String ObjName , String accessType, List<String> fieldList ) {
        List<String> returnFieldList = new List<String>();
        
        SObjectType sObjType = Schema.getGlobalDescribe().get(ObjName);
        Map<String , Schema.SObjectField> fieldMap = sObjType.getDescribe().fields.getMap();
        
        for(String field : fieldList){
            
            if(accessType.toLowerCase() == 'read' && fieldMap.get(field).getDescribe().isAccessible()) {
                returnFieldList.add(field);
            }
            else if(accessType.toLowerCase() == 'create' && fieldMap.get(field).getDescribe().isCreateable()) {
                returnFieldList.add(field);
            }
            else if(accessType.toLowerCase() == 'update' && fieldMap.get(field).getDescribe().isUpdateable()) {
                returnFieldList.add(field);
            }
        }
        return returnFieldList;
    }
    
    public static Boolean checkFieldAccess(String ObjName , String accessType, List<String> fieldList ) {
        Boolean isTrue = checkObjectAccess(ObjName,accessType);
        String message;
        List<String> restrictedFieldList = new List<String>();
        SObjectType sObjType = Schema.getGlobalDescribe().get(ObjName);
        Map<String , Schema.SObjectField> fieldMap = sObjType.getDescribe().fields.getMap();
        
        for(String field : fieldList){
            if(accessType.toLowerCase() == 'read') {
                isTrue = (isTrue && fieldMap.get(field).getDescribe().isAccessible());
                message = (isTrue) ? '' : 'You do not have permission to read the field {1} on {0}';
            }
            else if(accessType.toLowerCase() == 'create') {
                isTrue = (isTrue && fieldMap.get(field).getDescribe().isCreateable());
                message =  (isTrue) ? '' : 'You do not have permission to create field {1} on {0}';
            }
            else if(accessType.toLowerCase() == 'update') {
                isTrue = (isTrue && fieldMap.get(field).getDescribe().isUpdateable());
                message =  (isTrue) ? '' : 'You do not have permission to update the field {1} on {0}';  
            }
            if(!isTrue){
                restrictedFieldList.add(ObjName);
                restrictedFieldList.add(field);
                throw new securityException(String.format(message,restrictedFieldList));
            }  
        }
        return isTrue;
    }
    public class securityException extends Exception {} 
}