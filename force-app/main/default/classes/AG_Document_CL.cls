public with sharing class AG_Document_CL {
    
    public static Task task = new task();
    
    public static List<Task> taskList = new List<Task>();
    
    @AuraEnabled
    public static String getContacts(Id recordId){
        try{
            AG_Agreement__c agreement;
            List<EmailTemplate> emailTemplateList;
            List<ContactWrapper> contactWrapperList = new List<ContactWrapper>();
            List<ContentVersionWrapper> contentVersionWrapperList = new List<ContentVersionWrapper>();
            List<Id> contentDocumentIdList = new List<Id>();
            
            List<String> fieldListToRead = new List<String>{'AG_Account__c' , 'AG_Status__c', 'AG_Is_Changed__c'};
                List<String> fieldListToUpdate = new List<String>{'AG_Is_Changed__c'};
                    if(
                        AG_FLS_CL.checkFieldAccess('AG_Agreement__c', 'read', fieldListToRead)
                    ){
                        agreement = [SELECT Id, AG_Account__c, AG_Status__c, AG_Is_Changed__c FROM AG_Agreement__c WHERE Id =: recordId];
                        agreement.AG_Is_Changed__c = false;
                    }
            if(
                AG_FLS_CL.checkFieldAccess('AG_Agreement__c', 'update', fieldListToUpdate)
            ){
                update agreement;
            }
            
            if(agreement.AG_Status__c == 'In Review'){
                return '{"success": false, "message": "The agreement is already in review state"}';
            }
            else if(agreement.AG_Status__c == 'In Signature'){
                return '{"success": false, "message": "The agreement is already in the state of In Signature"}';
            } 
            else{
                callAgreementContacts(agreement, contactWrapperList);
                
                callAgreementDocuments(recordId, contentDocumentIdList);
                
                callContentVersion(contentVersionWrapperList, contentDocumentIdList);
                
                emailTemplateList = [SELECT Id,Name,Body,Subject,HTMLValue FROM EmailTemplate WHERE Folder.Name = 'Agreement Folder'];
                
                if(contentVersionWrapperList == null || contentVersionWrapperList.size()==0){
                    return '{"success": false, "message": "No Document(s) found to send"}';
                }
            }
            return '{"success": true, "data": '+JSON.serialize(contactWrapperList)+', "document": '+JSON.serialize(contentVersionWrapperList)+', "templateList": '+JSON.serialize(emailTemplateList)+'}';
        }
        catch(Exception e){
            System.debug(e.getLineNumber());
            System.debug(e.getMessage());
            return '{"success": false, "message": "'+e.getMessage()+'"}';
        }
    }
    
    private static List<ContactWrapper> callAgreementContacts(AG_Agreement__c agreement,List<ContactWrapper> contactWrapperList){
        for(Contact contact : [SELECT Id,Name,Email,FirstName,LastName FROM Contact WHERE AccountId = : agreement.AG_Account__c]){
            contactWrapperList.add(new ContactWrapper(false,contact));
        }
        return contactWrapperList;
    }
    
    private static List<Id> callAgreementDocuments(Id recordId, List<Id> contentDocumentIdList){
        for(contentDocumentLink contentDocLink : [SELECT LinkedEntityid, ContentDocumentid FROM contentDocumentLink WHERE LinkedEntityid =: recordId]){
            contentDocumentIdList.add(contentDocLink.ContentDocumentid);  
        }
        return contentDocumentIdList;
    }
    
    private static List<ContentVersionWrapper> callContentVersion(List<ContentVersionWrapper> contentVersionWrapperList, List<Id> contentDocumentIdList){
        for(ContentVersion contentVersion : [SELECT Title,IsLatest,FileExtension,ContentDocumentId,CreatedBy.Name,CreatedDate FROM ContentVersion WHERE ContentDocumentId IN : contentDocumentIdList AND IsLatest =: true]){
            contentVersionWrapperList.add(new ContentVersionWrapper(false,contentVersion));
        }
        return contentVersionWrapperList;
    }
    
    private static List<Id> callContentDocuments(Id recordId, List<Id> contentDocumentIdList, List<Id> contentDocIdList){
        for(contentDocumentLink contentDocLink : [SELECT LinkedEntityid, ContentDocumentid FROM contentDocumentLink WHERE LinkedEntityid =: recordId AND ContentDocumentid IN : contentDocIdList]){
            contentDocumentIdList.add(contentDocLink.ContentDocumentid);  
        }
        return contentDocumentIdList;
    }
    
    private static List<Messaging.EmailFileAttachment> callEmailAttachments(List<Id> contentDocumentIdList, List<Messaging.EmailFileAttachment> attachmentList){
        for(
            ContentVersion contentVersion : [SELECT Title,IsLatest,FileExtension,ContentDocumentId,VersionData FROM ContentVersion WHERE ContentDocumentId IN : contentDocumentIdList AND IsLatest =: true]
        ){
            Blob versionData = contentVersion.VersionData;
            Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment(); 
            attachment.setFileName(contentVersion.Title); 
            attachment.SetContentType('application/'+contentVersion.FileExtension+'');
            attachment.setBody(versionData);
            attachmentList.add(attachment);
        }
        return attachmentList;
    }
    
    private static List<EmailMessage> createEmailMessageList(List<String> ccAddresses, List<EmailTemplate> templateList, List<String> toAddressList){
        EmailMessage emailMessage = new EmailMessage();
        List<EmailMessage> emailMessageList = new List<EmailMessage>();
        if(ccAddresses != null){
            for(String ccAdres : ccAddresses){
                emailMessage.CcAddress = ccAdres;
            }
        }
        if(templateList.size() > 0){
            createBodyForEmailMessage(emailMessage, templateList);
        }
        if(toAddressList.size() > 0){
            for(String mail : toAddressList){
                emailMessage.ToAddress = mail;
            }
        }
        emailMessage.FromAddress = UserInfo.getUserEmail();
        emailMessage.MessageDate = Datetime.now();
        emailMessageList.add(emailMessage);
        if(emailMessageList.size() > 0){
            insert emailMessageList;
        }
        return emailMessageList;
    }
    
    private static EmailMessage createBodyForEmailMessage(EmailMessage emailMessage, List<EmailTemplate> templateList){
        for(EmailTemplate template : templateList){
            emailMessage.EmailTemplateId = template.Id;
            if(template.body != null){
                emailMessage.HtmlBody = template.body;
            }
            if(template.subject != null){
                emailMessage.Subject = template.subject;
            }
        }
        return emailMessage;
    }
    
    private static Messaging.SingleEmailMessage createEmailBody(Messaging.SingleEmailMessage email, List<EmailTemplate> templateList, String subject, String body, AG_Agreement__c agreement){
        if(templateList.size() > 0){
            for(EmailTemplate emailTemplate : templateList){
                email.setTemplateId(emailTemplate.Id);
                if(emailTemplate.subject != null){
                    email.setSubject(emailTemplate.subject);
                }
                if(emailTemplate.body != null){
                    String htmlBody = emailTemplate.HTMLValue;
                    htmlBody = htmlBody.replace('{!AG_Agreement__c.AG_Action__c}', agreement.AG_Action__c);
                    email.setHtmlBody(htmlBody);
                }
            }
        }
        else{
            if(subject != null || subject != ''){
                email.setSubject(subject);
            }
            if(body != null || body != ''){
                email.setHtmlBody(body);
            }
        }
        return email;
    }
    
    private static List<Id> getContentDocuments(List<Id> contentDocIdList , List<ContentVersion> contentVersionList){
        for(ContentVersion contentVersion : contentVersionList){
            contentDocIdList.add(contentVersion.ContentDocumentId);
        }
        return contentDocIdList;
    }
    
    private static List<String> getCCAdressses(List<String> ccAddresses, List<String> ccAddressList){
        for(String ccAdress : ccAddresses){
            List<String> formattedCCaddress = ccAdress.split(',');
            ccAddressList.addAll(formattedCCaddress);
        }
        return ccAddressList;
    }
    
    private static List<String> getToAdressses(List<String> toAddresses, List<String> toAddressList){
        for(String toAdress : toAddresses){
            toAddressList.add(toAdress);
        }
        return toAddressList;
    }
    
    private static String processEmailResultForReview(List<Messaging.SendEmailResult> emailResultList, AG_Agreement__c agreement, List<String> ccAddresses, List<String> toAddressList, List<EmailTemplate> templateList){
        List<String> fieldListToUpdate = new List<String>{'AG_Status__c'};
            for(Messaging.SendEmailResult result : emailResultList){
                if(result.success){
                    agreement.AG_Status__c = 'In Review';
                    task.Description = 'Document has been submitted For Review, Please check.';
                    task.Subject = 'Document Submitted For Review by '+UserInfo.getName();
                    task.ActivityDate = System.today();
                    task.Status = 'Completed';
                    task.WhatId = agreement.Id;
                    taskList.add(task);
                    createEmailMessageList(ccAddresses, templateList, toAddressList);
                }
                else{
                    return '{"success": false, "message": "Email sending failed"}';
                }
            }
        if(
            AG_FLS_CL.checkFieldAccess('AG_Agreement__c', 'update', fieldListToUpdate)
        ){
            update agreement;
        }
        if(taskList.size() > 0){
            insert taskList;
        }
        return '';
    }
    
    private static String processEmailResultForSignature(List<Messaging.SendEmailResult> emailResultList, AG_Agreement__c agreement, List<String> ccAddresses, List<String> toAddressList, List<EmailTemplate> templateList){
        List<String> fieldListToUpdate = new List<String>{'AG_Status__c'};
            for(Messaging.SendEmailResult result : emailResultList){
                if(result.success){
                    agreement.AG_Status__c = 'In Signature';
                    task.Description = 'Document has been sent for signature, Please check.';
                    task.Subject = 'Document Sent For Signature by '+UserInfo.getName();
                    task.ActivityDate = System.today();
                    task.Status = 'Completed';
                    task.WhatId = agreement.Id;
                    taskList.add(task);
                    createEmailMessageList(ccAddresses, templateList, toAddressList);
                }
                else{
                    return '{"success": false, "message": "Email sending failed"}';
                }
            }
        if(
            AG_FLS_CL.checkFieldAccess('AG_Agreement__c', 'update', fieldListToUpdate)
        ){
            update agreement;
        }
        if(taskList.size() > 0){
            insert taskList;
        }
        return '';
    }
    
    @AuraEnabled
    public static String sendForReview(Id recordId, String selectedContacts, String selectedDocuments, String templates, String toAddress, String ccAddress, String emailId, String subject, String body){
        System.debug('toAddress-->' +toAddress);
        try{ 
            AG_Agreement__c agreement;
            List<String> fieldListToRead = new List<String>{'AG_Account__c' , 'AG_Status__c', 'OwnerId'};
                if( 
                    AG_FLS_CL.checkFieldAccess('AG_Agreement__c', 'read', fieldListToRead)  
                ){  
                    agreement = [SELECT Id,AG_Agreement_Name__c,AG_Account__c,AG_Status__c,OwnerId,AG_Action__c FROM AG_Agreement__c WHERE Id =: recordId];
                }
            
            List<String> ccAddressList = new List<String>();
            List<String> toAddressList = new List<String>();
            List<String> ccAddresses = (List<String>) System.JSON.deserialize(ccAddress, List<String>.class);
            List<String> toAddresses = (List<String>) System.JSON.deserialize(toAddress, List<String>.class);
            
            getCCAdressses(ccAddresses, ccAddressList);
            getToAdressses(toAddresses, toAddressList);
            
            List<EmailTemplate> emailTemplateList = (List<EmailTemplate>) System.JSON.deserialize(templates, List<EmailTemplate>.class);
            List<EmailTemplate> templateList = [SELECT Id,Name,Body,Subject,HTMLValue FROM EmailTemplate WHERE Id =: emailId];
            
            List<OrgWideEmailAddress> orgWideList = [SELECT Id FROM OrgWideEmailAddress];
            Id orgWideAddressId = null;
            if(orgWideList.size() > 0){
                orgWideAddressId = orgWideList[0].Id;
            }
            
            List<Contact> contactList = (List<Contact>) System.JSON.deserialize(selectedContacts, List<Contact>.class);
            List<ContentVersion> contentVersionList = (List<ContentVersion>) System.JSON.deserialize(selectedDocuments, List<ContentVersion>.class);
            
            List<Id> contentDocIdList = new List<Id>();
            List<Id> contentDocumentIdList = new List<Id>();
            List<Messaging.EmailFileAttachment> attachmentList = new List<Messaging.EmailFileAttachment>();
            
            getContentDocuments(contentDocIdList, contentVersionList);
            callContentDocuments(recordId, contentDocumentIdList, contentDocIdList);
            callEmailAttachments(contentDocumentIdList, attachmentList);
            
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            if(Test.isRunningTest()){
                email.setTargetObjectId(agreement.OwnerId);
            }
            email.fileattachments = attachmentList;
            createEmailBody(email, templateList, subject, body, agreement);
            email.setOrgWideEmailAddressId(orgWideAddressId);
            email.setToAddresses(toAddressList); 
            email.setCcAddresses(ccAddressList);
            email.setWhatId(agreement.Id);
            if(!Test.isRunningTest()){
                email.setSaveAsActivity(true); 
            }
            else{
                email.setSaveAsActivity(false);
            }
            System.debug('email--->' +email.getToAddresses());
            List<Messaging.SendEmailResult> emailResultList = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
            if(emailResultList == null || emailResultList.size() == 0){
                return '{"success": false, "message": "Email sending failed"}';
            }
            else{
                processEmailResultForReview(emailResultList, agreement, ccAddresses, toAddressList, templateList);
                return '{"success": true, "message": "Email Sent successfully"}';
            } 
        }
        catch(Exception e){
            System.debug('getLineNo-->' +e.getLineNumber());
            System.debug('getMsg-->' +e.getMessage());
            return '{"success": false, "message": "'+e.getMessage()+'"}';
        }
    }
    
    @AuraEnabled
    public static String sendForSignature(Id recordId, String selectedContacts, String selectedDocuments, String templates, String toAddress, String ccAddress, String emailId, String subject, String body){
        try{ 
            AG_Agreement__c agreement;
            List<String> fieldListToRead = new List<String>{'AG_Account__c' , 'AG_Status__c', 'OwnerId'};
                if( 
                    AG_FLS_CL.checkFieldAccess('AG_Agreement__c', 'read', fieldListToRead)  
                ){  
                    agreement = [SELECT Id,AG_Account__c,AG_Status__c,OwnerId,AG_Action__c FROM AG_Agreement__c WHERE Id =: recordId];
                }
            List<String> ccAddressList = new List<String>();
            List<String> toAddressList = new List<String>();
            List<String> ccAddresses = (List<String>) System.JSON.deserialize(ccAddress, List<String>.class);
            List<String> toAddresses = (List<String>) System.JSON.deserialize(toAddress, List<String>.class);
            
            getCCAdressses(ccAddresses, ccAddressList);
            getToAdressses(toAddresses, toAddressList);
            
            List<EmailTemplate> emailTemplateList = (List<EmailTemplate>) System.JSON.deserialize(templates, List<EmailTemplate>.class);
            List<EmailTemplate> templateList = [SELECT Id,Name,Body,Subject,HTMLValue FROM EmailTemplate WHERE Id =: emailId];
            
            List<OrgWideEmailAddress> orgWideList = [SELECT Id FROM OrgWideEmailAddress];
            Id orgWideAddressId = null;
            if(orgWideList.size() > 0){
                orgWideAddressId = orgWideList[0].Id;
            }
            
            List<Contact> contactList = (List<Contact>) System.JSON.deserialize(selectedContacts, List<Contact>.class);
            List<ContentVersion> contentVersionList = (List<ContentVersion>) System.JSON.deserialize(selectedDocuments, List<ContentVersion>.class);
            
            List<Id> contentDocIdList = new List<Id>();
            List<Id> contentDocumentIdList = new List<Id>();
            List<Messaging.EmailFileAttachment> attachmentList = new List<Messaging.EmailFileAttachment>();
            
            getContentDocuments(contentDocIdList, contentVersionList);
            callContentDocuments(recordId, contentDocumentIdList, contentDocIdList);
            callEmailAttachments(contentDocumentIdList, attachmentList);
            
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            if(Test.isRunningTest()){
                email.setTargetObjectId(agreement.OwnerId);
            }
            email.fileattachments = attachmentList;
            createEmailBody(email, templateList, subject, body, agreement);
            email.setOrgWideEmailAddressId(orgWideAddressId);
            email.setToAddresses(toAddressList); 
            email.setCcAddresses(ccAddressList);
            email.setWhatId(agreement.Id);
            if(!Test.isRunningTest()){
                email.setSaveAsActivity(true); 
            }
            else{
                email.setSaveAsActivity(false);
            }
            
            List<Messaging.SendEmailResult> emailResultList = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
            if(emailResultList == null || emailResultList.size() == 0){
                return '{"success": false, "message": "Email sending failed"}';
            }
            else{
                processEmailResultForSignature(emailResultList, agreement, ccAddresses, toAddressList, templateList);
                return '{"success": true, "message": "Email Sent successfully"}';
            }
        }
        catch(Exception e){
            System.debug('getLineNo-->' +e.getLineNumber());
            System.debug('getMsg-->' +e.getMessage());
            return '{"success": false, "message": "'+e.getMessage()+'"}';
        }
    }
    
    @AuraEnabled
    public static String activateContract(Id recordId){
        try{
            AG_Agreement__c agreement;
            List<String> fieldListToRead = new List<String>{'AG_Account__c' , 'AG_Status__c'};
                List<String> fieldListToUpdate = new List<String>{'AG_Status__c', 'AG_Is_Changed__c'};
                    if(
                        AG_FLS_CL.checkFieldAccess('AG_Agreement__c', 'read', fieldListToRead)
                    ){
                        agreement = [SELECT Id,AG_Account__c,AG_Status__c,AG_Is_Changed__c FROM AG_Agreement__c WHERE Id =: recordId];
                    }
            
            if(agreement.AG_Status__c == 'In Signature'){
                agreement.AG_Is_Changed__c = false;
                agreement.AG_Status__c = 'Activated';
                if(
                    AG_FLS_CL.checkFieldAccess('AG_Agreement__c', 'read', fieldListToUpdate)
                ){
                    update agreement;
                }
                
                task.Description = 'Contract has been activated successfully';
                task.Subject = 'Contract activated successfully by '+UserInfo.getName();
                task.ActivityDate = System.today();
                task.Status = 'Completed';
                task.WhatId = agreement.Id;
                taskList.add(task);
                insert taskList;
            }
            else if(agreement.AG_Status__c == 'Activated'){
                return '{"success": false, "message": "Contract is already in activated state"}'; 
            }
            else{
                return '{"success": false, "message": "Contract cannot be activated"}';
            }
            return '{"success": true, "message": "Contract activated successfully"}';
        }
        catch(Exception e){
            System.debug(e.getLineNumber());
            System.debug(e.getMessage()); 
            return '{"success": false, "message": "'+e.getMessage()+'"}';
        }
    }
    
    @AuraEnabled
    public static String findAgreement(Id recordId){
        try{
            AG_Agreement__c agreement = getAgreementDetails(recordId);
            return '{"success": true, "data": '+JSON.serialize(agreement)+'}';
        }
        catch(Exception e){
            System.debug(e.getLineNumber());
            System.debug(e.getMessage()); 
            return '{"success": false, "message": "'+e.getMessage()+'"}';
        }
    }
    
    public static AG_Agreement__c getAgreementDetails(Id recordId){
        try{
            String query;
            List<String> fieldListToRead = new List<String>{'AG_Account__c' , 'Name', 'AG_Status__c', 'AG_Start_Date__c', 'AG_End_Date__c'};
                if(
                    AG_FLS_CL.checkFieldAccess('AG_Agreement__c', 'read', fieldListToRead)
                ){
                    query = 'SELECT Id,Name,AG_Status__c,AG_Start_Date__c,AG_End_Date__c,AG_Account__c FROM AG_Agreement__c WHERE Id =: recordId';
                }
            
            return Database.query(query);
        }
        catch(Exception e){
            System.debug(e.getLineNumber());
            System.debug(e.getMessage());
        }
        return null;
    }
    
    @AuraEnabled
    public static void createTask(Id recordId){
        task.Description = 'Document uploaded to send, Please check.';
        task.Subject = 'Document uploaded by '+UserInfo.getName();
        task.ActivityDate = System.today();
        task.Status = 'Completed';
        task.WhatId = recordId;
        taskList.add(task);
        insert taskList;
    }
    
    @AuraEnabled
    public static String validateUpload(Id recordId){
        try{
            Boolean isUpload = true;
            List<ContentVersion> contentVersionList = [SELECT Id,Title,IsLatest,FileExtension,ContentDocumentId,CreatedBy.Name,CreatedDate FROM ContentVersion WHERE FirstPublishLocationId =: recordId];
            if(contentVersionList.size() > 0){
                return '{"success": true, "message": "Multiple documents can\'t be added"}';
            }
            else{ 
                return '{"success": false, "data": '+isUpload+'}';
            }
        }
        catch(Exception e){
            System.debug(e.getLineNumber());
            System.debug(e.getMessage());
            return '{"success": false, "message": "'+e.getMessage()+'"}';
        }
    }
    
    @AuraEnabled
    public static String performBackProcess(Id recordId, String selectedValue, String commentedValue){
        try{
            AG_Agreement__c agreement;
            List<String> fieldListToRead = new List<String>{ 'AG_Status__c', 'AG_Is_Changed__c'};
                if(
                    AG_FLS_CL.checkFieldAccess('AG_Agreement__c', 'read', fieldListToRead)
                ){
                    agreement = [SELECT Id, AG_Status__c, AG_Is_Changed__c FROM AG_Agreement__c WHERE Id =: recordId];
                }
            
            if(agreement != null){
                agreement.AG_Is_Changed__c = true;
                if(selectedValue != 'In Approval'){
                    performBackProcessForAgreement(agreement,selectedValue);
                }
                else{
                    Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
                    request.setComments('Submitting request for approval.');
                    request.setObjectId(agreement.id);
                    request.setSubmitterId(UserInfo.getUserId()); 
                    request.setProcessDefinitionNameOrId('AG_Agreement_Approval_Process');
                    request.setSkipEntryCriteria(true);
                    Approval.ProcessResult result = Approval.process(request);
                }
                task.Description = commentedValue;
                task.Subject = 'Contract Process reverted by '+UserInfo.getName();
                task.ActivityDate = System.today();
                task.Status = 'Completed';
                task.WhatId = recordId;
                taskList.add(task);
                insert taskList;
            }
            return '{"success": true, "message": "Action performed successfully"}';
        }
        catch(Exception e){
            System.debug(e.getLineNumber());
            System.debug(e.getMessage());
            return '{"success": false, "message": "'+e.getMessage()+'"}';
        }
    }
    
    private static AG_Agreement__c performBackProcessForAgreement(AG_Agreement__c agreement, String selectedValue){
        if(selectedValue == 'Requested'){
            agreement.AG_Status__c = 'Requested';
        }
        else if(selectedValue == 'In Review'){
            agreement.AG_Status__c = 'In Review';
        }
        else if(selectedValue == 'In Signature'){
            agreement.AG_Status__c = 'In Signature';
        }
        update agreement;
        return agreement;
    }
    
    @AuraEnabled
    public static String checkBackProcessIsValid(Id recordId){
        Boolean isValid = true;
        AG_Agreement__c agreement = [SELECT Id, AG_Status__c FROM AG_Agreement__c WHERE Id =: recordId];
        if(agreement.AG_Status__c == 'Requested'){
            return '{"success": false, "message": "The contract is in its initial stage. Back process can\'t be performed at this stage"}';
        }
        return '{"success": true, "data": '+isValid+'}';
    }
    
    public class ContactWrapper {
        public Boolean isSelected;
        public Contact contacts;
        
        public ContactWrapper(Boolean isSelected, Contact contacts) {
            this.isSelected = isSelected;
            this.contacts = contacts;
        }
    }
    
    public class ContentVersionWrapper {
        public Boolean isSelected;
        public ContentVersion contentVersion;
        
        public ContentVersionWrapper(Boolean isSelected, ContentVersion contentVersion) {
            this.isSelected = isSelected;
            this.contentVersion = contentVersion;
        }
    } 
}