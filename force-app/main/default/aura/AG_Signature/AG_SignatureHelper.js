({
    navigateToReviewDocument : function(component) {
        var event = $A.get("e.force:navigateToComponent");
        event.setParams({
            componentDef : "c:AG_Send_For_Signature",
            componentAttributes: {
                recordId : component.get("v.recordId")
            }
        });
        event.fire();
    }
})