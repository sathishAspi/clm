({
    doInit : function(component, event, helper) {
        if(component.get("v.templateObject").Object_API_Name__c){
        component.set("v.showCancelButton",true)
        component.set("v.parentObject" , component.get("v.templateObject").Object_API_Name__c);
        component.set("v.pObjName" , component.get("v.templateObject").Object_API_Name__c);
           }
        var parentObj = component.get("v.parentObject");
        
        if(parentObj != '') {
            component.set("v.showUpdateButton",true);
            helper.getObjectList(component, true);
        } 
        else {
            component.set("v.showParentObject" ,true);
            helper.getObjectList(component, false);
        }
    },
    
    selectedObject : function(component, event, helper) {
        var resultValue = component.find("select").get("v.value");
        component.set("v.parentObject" , resultValue);
        component.set("v.pObjName" , resultValue);
    },
    
    nextButton : function(component, event, helper) {
        helper.getChildObjectList(component, event, helper);
    },
    
    selectedChildObject : function(component, event, helper) {
        component.set("v.isLoading",true);
        component.set("v.parentObjectFieldsList",[]);
        component.set("v.lookupFieldsList",[]);
        component.set("v.nextLookupFieldsList",[]);
        component.set("v.selectedFieldSet",'');
        component.set("v.finalSelectedFields",'');
        component.set("v.isFirstTime",false);
        component.set("v.isLookupFirstTime",false);
        
        var resultValue = component.find("child_select").get("v.value");
        if(resultValue == '') {
            component.set("v.pObjName" ,component.get("v.parentObject"));
            var listDataFilter = component.get("v.objList");
            var result = listDataFilter.filter(function(x) {
                return x.value == component.get("v.parentObject");
            });
            component.set("v.pObjLabel",result[0].label);
            component.set("v.isChildObjectAdded",false);
            component.set("v.selectedFieldSet", component.get("v.parentObject"));
            component.set("v.finalSelectedFields", '${' +component.get("v.parentObject")+ '}');
        }
        else {
            var listData = component.get("v.childObjectList");
            component.set("v.pObjName" ,resultValue);
            var res = listData.filter(function(x) {
                return x.value == resultValue;
            });
            component.set("v.pObjLabel",res[0].label);
           // component.set("v.childObjectLabel",res[0].label)
            component.set("v.isChildObjectAdded",true);
            component.set("v.selectedFieldSet", component.get("v.parentObject") + '.' +resultValue);
            component.set("v.finalSelectedFields", '$tbl{' +component.get("v.parentObject")+ '.' +resultValue+ '}');
        }
        
        var action = component.get("c.getParentObjFieldList"); 
        var objectName;
        if(resultValue == '') {
            objectName = component.get("v.parentObject");
        } else {
            objectName = resultValue;
        }
        var childObjectList = component.get("v.childObjectList");
        var apiName ='';
        childObjectList.forEach(function(x) {
                    if(x.value == objectName) {
                     console.log("api-->"+x.api);
                     apiName = x.api
                    }
        });
        action.setParams ({ "objName" : apiName });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if(state == "SUCCESS"){
                var objFieldsList = JSON.parse(response.getReturnValue());
                console.log("objFieldsList-->"+JSON.stringify(objFieldsList));
                objFieldsList.forEach(function(x) {
                    if(x.Type == 'REFERENCE') {
                        x.Label = x.Label + '>';
                    } 
                });
                component.set("v.parentObjectFieldsList",objFieldsList);
                component.set("v.isLoading",false);
            }
            component.set("v.isLoading",false);
        });
        $A.enqueueAction(action);
    },
    
    getLookupFields : function(component, event, helper) {
        var selectedField = component.find("InputSelect").get("v.value");
        var listData = component.get("v.parentObjectFieldsList");
        var resData = listData.filter(function(x) {
            return x.Name == selectedField;
        });
        var selField;
        console.log(resData[0].Type == 'REFERENCE');
        if(resData[0].Type == 'REFERENCE') {
            if(selectedField.endsWith('__c')) {
                selField = selectedField.slice(0,selectedField.length - 3) + '__r';
                
            } else {
                console.log("selectedField-->"+selectedField);
                var parentObjectFieldsList = component.get("v.parentObjectFieldsList");
                var res = parentObjectFieldsList.filter(function(x) {
                    return x.Name == selectedField;
                });
                console.log("res-->"+res);
                var fieldLabel = res[0].Label;
                var seleld = fieldLabel.slice(0,-1); 
                selField = seleld.trim();
            }
        }
        else {
            selField = component.find("InputSelect").get("v.value");
            
        }
        component.set("v.isLoading",true);
        component.set("v.lookupFieldsList",[]);
        component.set("v.nextLookupFieldsList",[]);
        component.set("v.isFirstTime",false);
        component.set("v.isLookupFirstTime",false);
        
        try {
            var isFirst = component.get("v.isFirst");
            if(!isFirst) {
                var isChildAdded = component.get("v.isChildObjectAdded");
                if(isChildAdded) {
                    var resultValue = component.get("v.selectedFieldSet");
                    component.set("v.selectedFieldSet",resultValue + '.' +selectedField);
                    component.set("v.finalSelectedFields",'$tbl{' +component.get("v.selectedFieldSet")+ '}');
                }
                else {
                    var result = component.get("v.selectedFieldSet");
                    component.set("v.selectedFieldSet",result + '.' +selectedField);
                    component.set("v.finalSelectedFields",'${' +component.get("v.selectedFieldSet")+ '}');
                }
                component.set("v.isFirst",true);
            }
            else {
                var isChildAddedTrue = component.get("v.isChildObjectAdded");
                if(isChildAddedTrue) {
                    var result = component.get("v.selectedFieldSet");
                    var firstIndex = result.indexOf('.');
                    var secondIndex = result.indexOf('.',firstIndex+1);
                    if(secondIndex == -1) {
                        component.set("v.selectedFieldSet",result + '.' +selectedField);
                        component.set("v.finalSelectedFields",'$tbl{' +component.get("v.selectedFieldSet")+ '}');
                    }
                    else {
                        var message = result.slice(0,secondIndex);
                        component.set("v.selectedFieldSet",message + '.' +selectedField);
                        component.set("v.finalSelectedFields",'$tbl{' +component.get("v.selectedFieldSet")+ '}');
                    }
                }
                else {
                    component.set("v.selectedFieldSet",component.get("v.pObjName") + '.' +selectedField);
                    component.set("v.finalSelectedFields",'${' +component.get("v.selectedFieldSet")+ '}');
                }
            }
        }
        catch(err) {
            console.log(err);
        }
        
        //component.set("v.selectedFieldSet",message);
        //component.set("v.finalSelectedFields", '${' + message + '}');
        
        var parentObjName = component.get("v.pObjName");
        var action = component.get("c.getLookupFieldsList");
        var childObjectList = component.get("v.childObjectList");
        var apiName ='';
        childObjectList.forEach(function(x) {
            if(x.value == parentObjName) {
                    apiName = x.api
                    }
        }); 
        console.log("objName-->"+parentObjName+','+selectedField);
        console.log("objName-->"+apiName);
        action.setParams ({ "objName" : apiName=='' ? parentObjName : apiName   , "fieldName" : selectedField });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if(state == 'SUCCESS') {
                try {
                    var res = JSON.parse(response.getReturnValue());
                    var fieldsList = res;
                    fieldsList.forEach(function(x) {
                        component.set("v.lookupObjectName",x.objName);
                        if(x.Type == 'REFERENCE') {
                            x.Label = x.Label + '>';
                        } 
                    });
                    var objList = component.get("v.objList");
                    var resData = objList.filter(function(x) {
                        return x.value == component.get("v.lookupObjectName");
                    });
                    component.set("v.lookupObjectLabel",resData[0].label);
                    component.set("v.lookupFieldsList", fieldsList);
                    component.set("v.isLoading",false);
                }
                catch(err) {
                    component.set("v.isLoading",false);
                }
                
            }else{
             component.set("v.isLoading",false);   
            }
        });
        $A.enqueueAction(action);
    },
    
    getNextLookupFields : function(component, event, helper) {
        var selectedField = component.find("LookupSelect").get("v.value");
        var listData = component.get("v.lookupFieldsList");
        var resData = listData.filter(function(x) {
            return x.Name == selectedField;
        });
        var selField;
        
        if(resData[0].Type == 'REFERENCE') {
            if(selectedField.endsWith('__c')) {
                selField = selectedField.slice(0,selectedField.length - 3) + '__r';
            } else {
                
                var lookupFieldsList = component.get("v.lookupFieldsList");
                var res = lookupFieldsList.filter(function(x) {
                    return x.Name == selectedField;
                });
                var fieldLabel = res[0].Label;
                
                var seleld = fieldLabel.slice(0,-1); 
                selField = seleld.trim();
                var objFieldName = selectedField;
                
             /*   if(selectedField.endsWith('Id')) {
                selectedField = selectedField.slice(0,selectedField.length - 2) + '';
            	} */
                
                
                
                //selField = selectedField;
            }
        }
        else {
            selField = component.find("LookupSelect").get("v.value");
        }
        component.set("v.nextLookupFieldsList",[]);
        component.set("v.isFirstTime",false);
        
        component.set("v.isLoading",true);
        var data = selectedField;
        var isFirst = component.get("v.isLookupFirstTime");
        var isChildAdded = component.get("v.isChildObjectAdded");
        
        if(!isFirst) {
            var selFieldSet = component.get("v.selectedFieldSet");
            if(isChildAdded) {
                var firstIndex = selFieldSet.indexOf('.');
                var secIndex = selFieldSet.indexOf('.',firstIndex+1);
                var thirdIndex = selFieldSet.indexOf('.',secIndex+1);
                if(thirdIndex == -1) {
                    selFieldSet = selFieldSet;
                    console.log("selectedFieldSet3-->"+selFieldSet + '.' +selectedField);
                    component.set("v.selectedFieldSet",selFieldSet.endsWith('__c') ? selFieldSet.slice(0,selFieldSet.length - 3) + '__r' + '.' +selectedField :selFieldSet + '.' +selectedField);
                    component.set("v.finalSelectedFields",'$tbl{' +component.get("v.selectedFieldSet")+ '}');
                }
                else {
                    selFieldSet = selFieldSet.slice('.',thirdIndex);
                    console.log("selectedFieldSet4-->"+selFieldSet + '.' +selectedField);
                    component.set("v.selectedFieldSet",selFieldSet + '.' +selectedField);
                    component.set("v.finalSelectedFields",'$tbl{' +component.get("v.selectedFieldSet")+ '}');
                }
            }
            else {
                var refSelectedField = component.get("v.selectedFieldSet");
                var message = refSelectedField.endsWith('__c') ? refSelectedField.slice(0,refSelectedField.length - 3) + '__r'+ '.' + data : refSelectedField + '.' + data;
                console.log("selectedFieldSet5-->"+message);
                component.set("v.selectedFieldSet",message);
                component.set("v.finalSelectedFields", '${' + message + '}');
            }
            component.set("v.isLookupFirstTime",true);
        } else {
            var selectFieldSet = component.get("v.selectedFieldSet");
            
            if(isChildAdded) {
                var firstIndex = selectFieldSet.indexOf('.');
                var secIndex = selectFieldSet.indexOf('.',firstIndex+1);
                var thirdIndex = selectFieldSet.indexOf('.',secIndex+1);
                if(thirdIndex == -1) {
                    selectFieldSet = selectFieldSet;
                    console.log("selectedFieldSet-->"+selectFieldSet + '.' +selectedField);
                    component.set("v.selectedFieldSet",selectFieldSet + '.' +selectedField);
                    component.set("v.finalSelectedFields",'$tbl{' +component.get("v.selectedFieldSet")+ '}');
                }
                else {
                    selectFieldSet = selectFieldSet.slice('.',thirdIndex);
                    console.log("selectedFieldSet2-->"+selectFieldSet + '.' +selectedField);
                    component.set("v.selectedFieldSet",selectFieldSet.endsWith('__c') ? selectFieldSet.slice(0,selectFieldSet.length - 3) + '__r' + '.' +selectedField : selectFieldSet + '.' +selectedField);
                    component.set("v.finalSelectedFields",'$tbl{' +component.get("v.selectedFieldSet")+ '}');
                }
            }
            else {
                var selectFieldSet = component.get("v.selectedFieldSet");
                var firstIndex = selectFieldSet.indexOf('.');
                var secIndex = selectFieldSet.indexOf('.',firstIndex+1);
                
                if(secIndex == -1) {
                    var message = component.get("v.selectedFieldSet")+ '.' + data;
                    console.log('secIndex1-->' +secIndex);
                    component.set("v.selectedFieldSet",message);
                    component.set("v.finalSelectedFields",'${' +message+ '}');
                }
                else {
                    var resultData = selectFieldSet.slice('.',secIndex);
                    console.log('secIndex2-->' +secIndex);
                    component.set("v.selectedFieldSet",resultData+ '.' +selectedField);
                    component.set("v.finalSelectedFields",'${' +component.get("v.selectedFieldSet")+ '}');
                }
            }
        }
        
        var objectName = component.get("v.lookupObjectName");
        var action = component.get("c.getLookupFieldsList");
        console.log("objectName-->"+objectName+','+selectedField);
        action.setParams({ "objName" : objectName , "fieldName" : selectedField });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if(state == 'SUCCESS') {
                try {
                    var res = JSON.parse(response.getReturnValue());
                    var fieldsList = res;
                    fieldsList.forEach(function(x) {
                        component.set("v.finalLookupObjectName",x.objName);
                    });
                    var objList = component.get("v.objList");
                    var resData = objList.filter(function(x) {
                        return x.value == component.get("v.finalLookupObjectName");
                    });
                    component.set("v.finalLookupObjectLabel",resData[0].label);
                    component.set("v.nextLookupFieldsList", res);
                    component.set("v.isLoading",false);
                }
                catch(err) {
                    console.log(err);
                    component.set("v.isLoading",false);
                }
            }else{
             component.set("v.isLoading",false);   
            }
        });
        $A.enqueueAction(action);
    },
    
    changeValue : function(component, event, helper) {
        component.set("v.isLoading",true);
        var data = component.find("Next_LookupSelect").get("v.value");
        var isFirst = component.get("v.isFirstTime");
        var isChildAdded = component.get("v.isChildObjectAdded");
        
        if(!isFirst) {
            var selFieldSet = component.get("v.selectedFieldSet");
            var message = selFieldSet.endsWith('__c') ? selFieldSet.slice(0,selFieldSet.length - 3) + '__r' + '.' + data :selFieldSet+ '.' +data;
            console.log("EXISTS")
            component.set("v.selectedFieldSet",message);
            if(isChildAdded) {
                component.set("v.finalSelectedFields", '$tbl{' + message + '}');
            }
            else {
                component.set("v.finalSelectedFields", '${' + message + '}');
            }
            component.set("v.isFirstTime",true);
        } else {
            var selFieldSet = component.get("v.selectedFieldSet");
            var index = selFieldSet.lastIndexOf('.');
            var result = selFieldSet.slice(0, index);
            var message = result + '.' + data;
            if(isChildAdded) {
                component.set("v.finalSelectedFields", '$tbl{' + message + '}');
            }
            else {
                component.set("v.finalSelectedFields", '${' + message + '}');
            }
        }
        component.set("v.isLoading",false);
    },
    
    copySelectedFieldSet : function(component, event, helper) {
        var copyText = component.get("v.finalSelectedFields");
        var hiddenInput = document.createElement("input");
        // passed text into the input
        hiddenInput.setAttribute("value", copyText);
        // Append the hiddenInput input to the body
        document.body.appendChild(hiddenInput);
        // select the content
        hiddenInput.select();
        // Execute the copy command
        document.execCommand("copy");
        // Remove the input from the body after copy text
        document.body.removeChild(hiddenInput); 
        // store target button label value
        var orignalLabel = event.getSource().get("v.label");
        // change button icon after copy text
        event.getSource().set("v.iconName" , 'utility:check');
        // change button label with 'copied' after copy text 
        event.getSource().set("v.label" , 'copied');
        
        // set timeout to reset icon and label value after 700 milliseconds 
        setTimeout(function(){ 
            event.getSource().set("v.iconName" , 'utility:copy_to_clipboard'); 
            event.getSource().set("v.label" , orignalLabel);
        }, 700);
    },
    
    updateFormat : function(component, event, helper) {
        var tempId = component.get("v.templateId");
        var form = component.get("v.selectedFieldSet");
        var action = component.get("c.updateTemplateFormat");
        action.setParams ({ "templateId" : tempId, "Format" : form });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if(state == 'SUCCESS') {
                var res = JSON.parse(response.getReturnValue());
                var toastEvent = $A.get("e.force:showToast");
                if(res.success) {
                    
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": res.message
                    });
                    toastEvent.fire();
                }
                else {
                    
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": res.message
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },
    previousPage :function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.templateObject").Id
            
        });
        navEvt.fire();
    }
})