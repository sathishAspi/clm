({
    getSelectedDocument : function(component) {
        var documentList = component.get("v.documentList");
        var document = component.find("documentTwo").get("v.value");
        var selectedDocument = documentList.filter(doc => doc.Title == document)[0];
        component.set("v.selectedDocumentTwo" , selectedDocument);
    },
    
    navigate : function(component) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId"),
            "slideDevName": "details"
        });
        navEvt.fire();
    }
})